#!/usr/bin/env bash
VER="1.0"
# Translation
source /usr/bin/rb-workflow-settings_lang.sh
# File paths
DAILY_CONFIG_DESKTOP_PATH="$HOME/.config/workflow-settings/daily-desktop"
DAILY_CONFIG_SERVICE_PATH="$HOME/.config/workflow-settings/daily-service"
DAILY_SYSTEM_TWEAKS_PATH="$HOME/.config/workflow-settings/daily-tweaks"
DAILY_GOVERNOR_PATH="$HOME/.config/workflow-settings/daily-governor"
STUDIO_CONFIG_DESKTOP_PATH="$HOME/.config/workflow-settings/studio-desktop"
STUDIO_CONFIG_SERVICE_PATH="$HOME/.config/workflow-settings/studio-service"
STUDIO_SYSTEM_TWEAKS_PATH="$HOME/.config/workflow-settings/studio-tweaks"
STUDIO_GOVERNOR_PATH="$HOME/.config/workflow-settings/studio-governor"
CONF_PATH="$HOME/.config/workflow-settings/conf"
ICON_PATH="/usr/share/icons/hicolor/scalable/apps/recbox-workflow-settings.svg"
# Menu widgets
SETTUBGS_MAIN_MENU="zenity --list --width=544 --height=250 --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --column="$OPTIONS_COLUMN_TITLE" \
	--column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --window-icon="$ICON_PATH""
SETTINGS_SUB_MENU="zenity --list --width=572 --height=230 --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" --column="$OPTIONS_COLUMN_TITLE"\
	--column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --window-icon="$ICON_PATH""
DESKTOP_MENU="zenity --list --checklist --multiple --width="400" --height="400" \
	--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" \
	--column="$SEPARATOR" --column="$OPTIONS_COLUMN_TITLE" --window-icon="$ICON_PATH""
SERVICE_MENU="zenity --list --checklist --multiple --width=685 --height=380 --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" --column="$SEPARATOR" \
	--column="$OPTIONS_COLUMN_TITLE" --column="$SEPARATOR" --column="$NAME_COLUMN_TITLE" \
	--column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --window-icon="$ICON_PATH""
TWEAKS_MENU="zenity --list --checklist --multiple --width=685 --height=380 --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" --column="$SEPARATOR" \
	--column="$OPTIONS_COLUMN_TITLE" --column="$SEPARATOR" --column="$NAME_COLUMN_TITLE" \
	--column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --window-icon="$ICON_PATH""
GOVERNOR_MENU="zenity --list --radiolist --width="585" --height="290" --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" --column="$SEPARATOR" \
	--column="$OPTIONS_COLUMN_TITLE" --column="$SEPARATOR" --column="$NAME_COLUMN_TITLE" \
	--column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --window-icon="$ICON_PATH""

settings() {
MAIN_BOX=$($SETTUBGS_MAIN_MENU --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_TEXT" \
	"$DAILY_OPTION" "$SEPARATOR" "$DAILY_OPTION_DESC" \
	"$STUDIO_OPTION" "$SEPARATOR" "$STUDIO_OPTION_DESC" \
	"$RESTART_OPTION" "$SEPARATOR" "$RESTART_OPTION_DESC" \
	"$DOCS_OPTION" "$SEPARATOR" "$DOCS_OPTION_DESC" \
	"$ABOUT_OPTION" "$SEPARATOR" "$ABOUT_DESC")
	if [[ -z MAIN_$BOX ]]; then
		exit 0
	elif [[ $MAIN_BOX == "$DAILY_OPTION" ]]; then
		if [[ -z $(find $HOME/.config/workflow-settings/ -name daily-desktop) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; echo " -> $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"
			grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | sed 's/:Hidden//' | cat > $DAILY_CONFIG_DESKTOP_PATH
		else
			echo ":: $AUTOSTART_OUTPUT_CONFIGURED_DAILY"; exec rb-workflow-settings.sh --daily-submenu
		fi
		if [[ -z $(find $HOME/.config/workflow-settings/ -name daily-service) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; echo " -> $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"
			echo -e "cpupower_scaling=Default\ntlp_ondemand=Default" >> $DAILY_CONFIG_SERVICE_PATH
			exec rb-workflow-settings.sh --daily-submenu
		else
			echo ":: $SERVICE_OUTPUT_CONFIGURED_DAILY"; exec rb-workflow-settings.sh --daily-submenu
		fi
		if [[ -z $(find $HOME/.config/workflow-settings/ -name daily-tweaks) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"
			reconf_dtweaks; exec rb-workflow-settings.sh --daily-submenu
		else
			echo ":: $TWEAKS_OUTPUT_CONFIGURED_DAILY"; exec rb-workflow-settings.sh --daily-submenu
		fi
	elif [[ $MAIN_BOX == "$STUDIO_OPTION" ]]; then
		if [[ -z $(find $HOME/.config/workflow-settings/ -name studio-desktop) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; echo " -> $AUTOSTART_OUTPUT_CREATING_CONFIG_STUDIO"
			grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | cat > $DAILY_CONFIG_DESKTOP_PATH $STUDIO_CONFIG_DESKTOP_PATH
		else
			echo ":: $AUTOSTART_OUTPUT_CONFIGURED_STUDIO"; exec rb-workflow-settings.sh --studio-submenu
		fi
		if [[ -z $(find $HOME/.config/workflow-settings/ -name studio-service) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; " -> $SERVICE_OUTPUT_CREATING_CONFIG_STUDIO"
			echo -e "cpupower_scaling=Default\ntlp_ondemand=Default" >> $STUDIO_CONFIG_SERVICE_PATH
			exec rb-workflow-settings.sh --studio-submenu
		else
			echo ":: $SERVICE_OUTPUT_CONFIGURED_STUDIO"; exec rb-workflow-settings.sh --studio-submenu
		fi
		if [[ -z $(find $HOME/.config/workflow-settings/ -name studio-tweaks) ]]; then
			echo ":: $DST_OUTPUT_CONFIG_NOTEXIST"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_STUDIO"
			reconf_stweaks; exec rb-workflow-settings.sh --studio-submenu
		else
			echo ":: $TWEAKS_OUTPUT_CONFIGURED_STUDIO"; exec rb-workflow-settings.sh --studio-submenu
		fi
	elif [[ $MAIN_BOX == "$RESTART_OPTION" ]]; then
		reconf_sservices ;reconf_dservices; reconf_dtweaks; reconf_stweaks; reconf_dgovernor; reconf_sgovernor
		zenity --info --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_INFO_TEXT" \
		--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
		exec rb-workflow-settings.sh -sS
	elif [[ $MAIN_BOX == "$DOCS_OPTION" ]]; then
		xdg-open /usr/share/doc/recbox/Workflow\ documentation.pdf; rb-workflow-settings.sh -S
	elif [[ $MAIN_BOX == "$ABOUT_OPTION" ]]; then
		zenity --info --icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" \
		--width="400" --title="$ABOUT_TITLE" --text="<b>RecBox Workflow $VER</b>\n\n$ABOUT_TEXT"
		rb-workflow-settings.sh -S
	fi
}

daily_submenu() {
BOX=$($SETTINGS_SUB_MENU --title="$DAILY_SUBMENU_TITLE" --text="$DAILY_SUBMENU_TEXT" \
	"$SETUP_DESKTOP" "$SEPARATOR" "$SETUP_DESKTOP_DESC" \
	"$SETUP_SERVICES" "$SEPARATOR" "$SETUP_SERVICES_DESC" \
	"$SYSTEM_TWEAKS" "$SEPARATOR" "$SYSTEM_TWEAKS_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX == "$SETUP_DESKTOP" ]]; then
		reconf_ddesktop; settings_daily_desktop_menu
	elif [[ $BOX == "$SETUP_SERVICES" ]]; then
		settings_daily_service_menu
	elif [[ $BOX == "$SYSTEM_TWEAKS" ]]; then
		settings_daily_tweaks_menu
	elif [[ $BOX == $EXTRA_BUTTON ]]; then
		exec rb-workflow-settings.sh -sS
	fi
}

studio_submenu() {
BOX=$($SETTINGS_SUB_MENU --title="$STUDIO_SUBMENU_TITLE" --text="$STUDIO_SUBMENU_TEXT" \
	"$SETUP_DESKTOP" "$SEPARATOR" "$SETUP_DESKTOP_DESC" \
	"$SETUP_SERVICES" "$SEPARATOR" "$SETUP_SERVICES_DESC" \
	"$SYSTEM_TWEAKS" "$SEPARATOR" "$SYSTEM_TWEAKS_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX == "$SETUP_DESKTOP" ]]; then
		reconf_sdesktop; settings_studio_desktop_menu
	elif [[ $BOX == "$SETUP_SERVICES" ]]; then
		settings_studio_service_menu
	elif [[ $BOX == "$SYSTEM_TWEAKS" ]]; then
		settings_studio_tweaks_menu
	elif [[ $BOX == $EXTRA_BUTTON ]]; then
		exec rb-workflow-settings.sh -sS
	fi
}

mode_test() {
if [[ $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') == "daily" ]]; then
	rb-workflow-desktop.sh -d
elif [[ $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') == "studio" ]]; then
	rb-workflow-desktop.sh -s
fi
}

settings_daily_desktop_menu() {
TRUE=$(grep -w 'false' $DAILY_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}' | sed 's/^/false /')
FALSE=$(grep -w 'true' $DAILY_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}' | sed 's/^/true /')
BOX=$($DESKTOP_MENU --title="$DAILY_DESKTOP_MENU_TITLE" --text="$DAILY_DESKTOP_MENU_TEXT" $FALSE $TRUE --separator=" ")
	if [[ -z $BOX ]]; then
		mode_test; exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			mode_test; daily_submenu
		else
			# Set all options to true (dirty way to set options to true)
			sed -i 's/true/false/' $DAILY_CONFIG_DESKTOP_PATH
			# Seting options marked by user to false
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $DAILY_CONFIG_DESKTOP_PATH
			done
			mode_test; daily_submenu
		fi
	fi
}

settings_studio_desktop_menu() {
TRUE=$(grep -w 'false' $STUDIO_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}' | sed 's/^/false /')
FALSE=$(grep -w 'true' $STUDIO_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}' | sed 's/^/true /')
BOX=$($DESKTOP_MENU --title="$STUDIO_DESKTOP_MENU_TITLE" --text="$DAILY_DESKTOP_MENU_TEXT" $FALSE $TRUE --separator=" ")
	if [[ -z $BOX ]]; then
		mode_test; exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			mode_test; studio_submenu
		else
			# Set all options to true (dirty way to set options to true)
			sed -i 's/true/false/' $STUDIO_CONFIG_DESKTOP_PATH
			# Seting options marked by user to false
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $STUDIO_CONFIG_DESKTOP_PATH
			done
			mode_test; studio_submenu
		fi
	fi
}

settings_daily_service_menu() {
BOX=$($SERVICE_MENU --title="$DAILY_SERVICE_MENU_TITLE" --text="$DAILY_SERVICE_MENU_TEXT" --separator="\n" \
	$(grep -w 'bluetooth.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "bluetooth.service" "$SEPARATOR" "$BLUETOOTH_SERVICE_OPTION" "$SEPARATOR" "$BLUETOOTH_SERVICE_DESC" \
	$(grep -w 'cups.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cups.service" "$SEPARATOR" "$CUPS_SERVICE_OPTION" "$SEPARATOR" "$CUPS_SERVICE_DESC" \
	$(grep -w 'cpupower.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cpupower.service" "$SEPARATOR" "$CPUPOWER_SERVICE_OPTION" "$SEPARATOR" "$CPUPOWER_SERVICE_DESC" \
	$(grep -w 'tlp.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "tlp.service" "$SEPARATOR" "$TLP_SERVICE_OPTION" "$SEPARATOR" "$TLP_SERVICE_DESC" \
	$(grep -w 'hpet.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "hpet.service" "$SEPARATOR" "$HPET_SERVICE_OPTION" "$SEPARATOR" "$HPET_SERVICE_DESC" \
	$(grep -w 'rtc.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "rtc.service" "$SEPARATOR" "$RTC_SERVICE_OPTION" "$SEPARATOR" "$RTC_SERVICE_DESC" \
	$(grep -w 'ufw.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ufw.service" "$SEPARATOR" "$UFW_SERVICE_OPTION" "$SEPARATOR" "$UFW_SERVICE_DESC" \
	$(grep -w 'firewalld.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "firewalld.service" "$SEPARATOR" "$FIREWALLD_SERVICE_OPTION" "$SEPARATOR" "$FIREWALLD_SERVICE_DESC" \
	$(grep -w 'libvirtd.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "libvirtd.service" "$SEPARATOR" "$VIRTMANAGER_SERVICE_OPTION" "$SEPARATOR" "$VIRTMANAGER_SERVICE_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			daily_submenu
		else
			# Restart config file
			sed -i 's/true/false/' $DAILY_CONFIG_SERVICE_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $DAILY_CONFIG_SERVICE_PATH
			done
		fi
		# cpupower and tlp service check
		TLP=$(grep -w 'tlp.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
		CPUPOWER=$(grep -w 'cpupower.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
		if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
			zenity --info --title="$DAILY_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_1" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_daily_service_menu
		else
			# ufw and firewalld service check
			UFW=$(grep -w 'ufw.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
			FIREWALLD=$(grep -w 'firewalld.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
			if [[ $UFW == "true" ]] && [[ $FIREWALLD == "true" ]]; then
				zenity --info --title="$DAILY_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_2" \
				--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
				settings_daily_service_menu
			else
				daily_submenu
			fi
		fi
	fi
}

settings_studio_service_menu() {
BOX=$($SERVICE_MENU --title="$STUDIO_SERVICE_MENU_TITLE" --text="$STUDIO_SERVICE_MENU_TEXT" --separator="\n" \
	$(grep -w 'bluetooth.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "bluetooth.service" "$SEPARATOR" "$BLUETOOTH_SERVICE_OPTION" "$SEPARATOR" "$BLUETOOTH_SERVICE_DESC" \
	$(grep -w 'cups.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cups.service" "$SEPARATOR" "$CUPS_SERVICE_OPTION" "$SEPARATOR" "$CUPS_SERVICE_DESC" \
	$(grep -w 'cpupower.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "cpupower.service" "$SEPARATOR" "$CPUPOWER_SERVICE_OPTION" "$SEPARATOR" "$CPUPOWER_SERVICE_DESC" \
	$(grep -w 'tlp.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "tlp.service" "$SEPARATOR" "$TLP_SERVICE_OPTION" "$SEPARATOR" "$TLP_SERVICE_DESC" \
	$(grep -w 'hpet.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "hpet.service" "$SEPARATOR" "$HPET_SERVICE_OPTION" "$SEPARATOR" "$HPET_SERVICE_DESC" \
	$(grep -w 'rtc.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "rtc.service" "$SEPARATOR" "$RTC_SERVICE_OPTION" "$SEPARATOR" "$RTC_SERVICE_DESC" \
	$(grep -w 'ufw.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ufw.service" "$SEPARATOR" "$UFW_SERVICE_OPTION" "$SEPARATOR" "$UFW_SERVICE_DESC" \
	$(grep -w 'firewalld.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "firewalld.service" "$SEPARATOR" "$FIREWALLD_SERVICE_OPTION" "$SEPARATOR" "$FIREWALLD_SERVICE_DESC" \
	$(grep -w 'libvirtd.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "libvirtd.service" "$SEPARATOR" "$VIRTMANAGER_SERVICE_OPTION" "$SEPARATOR" "$VIRTMANAGER_SERVICE_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			studio_submenu
		else
			# Restart config file
			sed -i 's/true/false/' $STUDIO_CONFIG_SERVICE_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $STUDIO_CONFIG_SERVICE_PATH
			done
		fi
		# cpupower and tlp service check
		TLP=$(grep -w 'tlp.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
		CPUPOWER=$(grep -w 'cpupower.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
		if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
			zenity --info --title="$STUDIO_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_1" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_studio_service_menu
		else
			# ufw and firewalld service check
			UFW=$(grep -w 'ufw.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
			FIREWALLD=$(grep -w 'firewalld.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
			if [[ $UFW == "true" ]] && [[ $FIREWALLD == "true" ]]; then
				zenity --info --title="$STUDIO_SERVICE_MENU_TITLE" --text="$SERVICE_MENU_INFO_DIALOG_TEXT_2" \
				--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
				settings_studio_service_menu
			else
				studio_submenu
			fi
		fi
	fi
}

settings_daily_tweaks_menu() {
BOX=$($TWEAKS_MENU --title="$DAILY_TWEAKS_MENU_TITLE" --text="$DAILY_TWEAKS_MENU_TEXT" --separator="\n" \
	$(grep -w 'audio_group' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "audio_group" "$SEPARATOR" "$AUDIO_GROUP_OPTION" "$SEPARATOR" "$AUDIO_GROUP_DESC" \
	$(grep -w 'realtime_group' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "realtime_group" "$SEPARATOR" "$REALTIME_GROUP_OPTION" "$SEPARATOR" "$REALTIME_GROUP_DESC" \
	$(grep -w 'swappiness' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "swappiness" "$SEPARATOR" "$SWAPPINESS_OPTION" "$SEPARATOR" "$SWAPPINESS_DESC" \
	$(grep -w 'maximum_watches' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "maximum_watches" "$SEPARATOR" "$MAXIMUM_WATCHES_OPTION" "$SEPARATOR" "$MAXIMUM_WATCHES_DESC" \
	$(grep -w 'frequency_scaling_cpu' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_cpu" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_DESC" \
	$(grep -w 'frequency_scaling_tlp' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_tlp" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_DESC" \
	$(grep -w 'threadirqs' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "threadirqs" "$SEPARATOR" "$THREADIRQS_OPTION" "$SEPARATOR" "$THREADIRQS_DESC" \
	$(grep -w 'unlock_memory' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "unlock_memory" "$SEPARATOR" "$UNLOCK_MEMORY_OPTION" "$SEPARATOR" "$UNLOCK_MEMORY_DESC" \
	$(grep -w 'open_files_limit' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "open_files_limit" "$SEPARATOR" "$OPEN_FILES_LIMIT_OPTION" "$SEPARATOR" "$OPEN_FILES_LIMIT_DESC" \
	$(grep -w 'precision_event_timer' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "precision_event_timer" "$SEPARATOR" "$PRECISION_EVENT_TIMER_OPTION" "$SEPARATOR" "$PRECISION_EVENT_TIMER_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			daily_submenu
		else
			# Restart config file
			sed -i 's/true/false/' $DAILY_SYSTEM_TWEAKS_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $DAILY_SYSTEM_TWEAKS_PATH
			done
		fi
		# Governor menu
		TLP=$(grep -w 'frequency_scaling_tlp' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}')
		CPUPOWER=$(grep -w 'frequency_scaling_cpu' $DAILY_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}')
		if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
			zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_2" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_daily_tweaks_menu
		elif [[ $TLP == "false" ]] && [[ $CPUPOWER == "false" ]]; then
			zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_1" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_daily_tweaks_menu
		elif [[ $TLP == "true" ]]; then
			daily_governor_menu
		elif [[ $CPUPOWER == "true" ]]; then
			daily_governor_menu
		else
			daily_submenu
		fi
	fi
}

settings_studio_tweaks_menu() {
BOX=$($TWEAKS_MENU --title="$STUDIO_TWEAKS_MENU_TITLE" --text="$STUDIO_TWEAKS_MENU_TEXT" --separator="\n" \
	$(grep -w 'audio_group' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "audio_group" "$SEPARATOR" "$AUDIO_GROUP_OPTION" "$SEPARATOR" "$AUDIO_GROUP_DESC" \
	$(grep -w 'realtime_group' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "realtime_group" "$SEPARATOR" "$REALTIME_GROUP_OPTION" "$SEPARATOR" "$REALTIME_GROUP_DESC" \
	$(grep -w 'swappiness' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "swappiness" "$SEPARATOR" "$SWAPPINESS_OPTION" "$SEPARATOR" "$SWAPPINESS_DESC" \
	$(grep -w 'maximum_watches' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "maximum_watches" "$SEPARATOR" "$MAXIMUM_WATCHES_OPTION" "$SEPARATOR" "$MAXIMUM_WATCHES_DESC" \
	$(grep -w 'frequency_scaling_cpu' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_cpu" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_CPU_POWER_DESC" \
	$(grep -w 'frequency_scaling_tlp' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "frequency_scaling_tlp" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_OPTION" "$SEPARATOR" "$FREQUENCY_SCALING_TLP_DESC" \
	$(grep -w 'threadirqs' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "threadirqs" "$SEPARATOR" "$THREADIRQS_OPTION" "$SEPARATOR" "$THREADIRQS_DESC" \
	$(grep -w 'unlock_memory' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "unlock_memory" "$SEPARATOR" "$UNLOCK_MEMORY_OPTION" "$SEPARATOR" "$UNLOCK_MEMORY_DESC" \
	$(grep -w 'open_files_limit' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "open_files_limit" "$SEPARATOR" "$OPEN_FILES_LIMIT_OPTION" "$SEPARATOR" "$OPEN_FILES_LIMIT_DESC" \
	$(grep -w 'precision_event_timer' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "precision_event_timer" "$SEPARATOR" "$PRECISION_EVENT_TIMER_OPTION" "$SEPARATOR" "$PRECISION_EVENT_TIMER_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			studio_submenu
		else
			# Restart config file
			sed -i 's/true/false/' $STUDIO_SYSTEM_TWEAKS_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $STUDIO_SYSTEM_TWEAKS_PATH
			done
		fi
		# Governor menu
		TLP=$(grep -w 'frequency_scaling_tlp' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}')
		CPUPOWER=$(grep -w 'frequency_scaling_cpu' $STUDIO_SYSTEM_TWEAKS_PATH | awk -F '=' '{print $2}')
		if [[ $TLP == "true" ]] && [[ $CPUPOWER == "true" ]]; then
			zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_2" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_studio_tweaks_menu
		elif [[ $TLP == "false" ]] && [[ $CPUPOWER == "false" ]]; then
			zenity --info --title="$GOV_MENU_TITLE" --text="$GOV_MENU_INFO_DIALOG_TEXT_1" \
			--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
			settings_studio_tweaks_menu
		elif [[ $TLP == "true" ]]; then
			studio_governor_menu
		elif [[ $CPUPOWER == "true" ]]; then
			studio_governor_menu
		else
			studio_submenu
		fi
	fi
}

daily_governor_menu() {
BOX=$($GOVERNOR_MENU --title="$GOV_MENU_TITLE" --text="$GOV_MENU_TEXT" --separator="\n" \
	$(grep -w 'gov_ondemand' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ondemand" "$SEPARATOR" "$ONDEMAND_OPTION" "$SEPARATOR" "$ONDEMAND_DESC" \
	$(grep -w 'gov_powersave' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "powersave" "$SEPARATOR" "$POWERSAVE_OPTION" "$SEPARATOR" "$POWERSAVE_DESC" \
	$(grep -w 'gov_userspace' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "userspace" "$SEPARATOR" "$USERSPACE_OPTION" "$SEPARATOR" "$USERSPACE_DESC" \
	$(grep -w 'gov_performance' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "performance" "$SEPARATOR" "$PERFORMANCE_OPTION" "$SEPARATOR" "$PERFORMANCE_DESC" \
	$(grep -w 'gov_conservative' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "conservative" "$SEPARATOR" "$CONSERVATIVE_OPTION" "$SEPARATOR" "$CONSERVATIVE_DESC" \
	$(grep -w 'gov_schedutil' $DAILY_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "schedutil" "$SEPARATOR" "$SCHEDUTIL_OPTION" "$SEPARATOR" "$SCHEDUTIL_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			settings_daily_tweaks_menu
		else
			# Restart config file
			sed -i 's/true/false/' $DAILY_GOVERNOR_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $DAILY_GOVERNOR_PATH
			done
			daily_submenu
		fi
	fi
}

studio_governor_menu() {
BOX=$($GOVERNOR_MENU --title="$GOV_MENU_TITLE" --text="$GOV_MENU_TEXT" --separator="\n" \
$(grep -w 'gov_ondemand' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "ondemand" "$SEPARATOR" "$ONDEMAND_OPTION" "$SEPARATOR" "$ONDEMAND_DESC" \
$(grep -w 'gov_powersave' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "powersave" "$SEPARATOR" "$POWERSAVE_OPTION" "$SEPARATOR" "$POWERSAVE_DESC" \
$(grep -w 'gov_userspace' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "userspace" "$SEPARATOR" "$USERSPACE_OPTION" "$SEPARATOR" "$USERSPACE_DESC" \
$(grep -w 'gov_performance' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "performance" "$SEPARATOR" "$PERFORMANCE_OPTION" "$SEPARATOR" "$PERFORMANCE_DESC" \
$(grep -w 'gov_conservative' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "conservative" "$SEPARATOR" "$CONSERVATIVE_OPTION" "$SEPARATOR" "$CONSERVATIVE_DESC" \
$(grep -w 'gov_schedutil' $STUDIO_GOVERNOR_PATH | awk -F '=' '{print $2}' | awk '{print toupper($0)}') "schedutil" "$SEPARATOR" "$SCHEDUTIL_OPTION" "$SEPARATOR" "$SCHEDUTIL_DESC")
	if [[ -z $BOX ]]; then
		exit 0
	elif [[ $BOX ]]; then
		if [[ $BOX == $EXTRA_BUTTON ]]; then
			settings_studio_tweaks_menu
		else
			# Restart config file
			sed -i 's/true/false/' $STUDIO_GOVERNOR_PATH
			# Add new configuration
			for i in $BOX; do
				sed -i "/$i/ s/false/true/" $STUDIO_GOVERNOR_PATH
			done
			studio_submenu
		fi
	fi
}

sync_configs() {
if [[ -z $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') ]]; then
	echo "mode=daily" > $CONF_PATH
fi
### Autostart configuration when file doesn't exist or empty.
if [[ -z $(find $HOME/.config/workflow-settings) ]]; then
	mkdir $HOME/.config/workflow-settings
fi
# Session and Startup
if [[ -f $DAILY_CONFIG_DESKTOP_PATH ]]; then
	if [[ -s $DAILY_CONFIG_DESKTOP_PATH ]]; then
		echo ":: $AUTOSTART_OUTPUT_CONFIGURED_DAILY"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"; reconf_ddesktop
	fi
else
	echo ":: $AUTOSTART_OUTPUT_CREATING_CONFIG_DAILY"; reconf_ddesktop
fi
if [[ -f $STUDIO_CONFIG_DESKTOP_PATH ]]; then
	if [[ -s $STUDIO_CONFIG_DESKTOP_PATH ]]; then
		echo ":: $AUTOSTART_OUTPUT_CONFIGURED_STUDIO"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $AUTOSTART_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_sdesktop
	fi
else
	echo ":: $AUTOSTART_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_sdesktop
fi
# Services
if [[ -f $DAILY_CONFIG_SERVICE_PATH ]]; then
	if [[ -s $DAILY_CONFIG_SERVICE_PATH ]]; then
		echo ":: $SERVICE_OUTPUT_CONFIGURED_DAILY"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"; reconf_dservices
	fi
else
	echo ":: $SERVICE_OUTPUT_CREATING_CONFIG_DAILY"; reconf_dservices
fi
if [[ -f $STUDIO_CONFIG_SERVICE_PATH ]]; then
	if [[ -s $STUDIO_CONFIG_SERVICE_PATH ]]; then
		echo ":: $SERVICE_OUTPUT_CONFIGURED_STUDIO"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $SERVICE_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_sservices
	fi
else
	echo ":: $SERVICE_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_sservices
fi
# System tweaks
if [[ -f $DAILY_SYSTEM_TWEAKS_PATH ]]; then
	if [[ -s $DAILY_SYSTEM_TWEAKS_PATH ]]; then
		echo ":: $TWEAKS_OUTPUT_CONFIGURED_DAILY"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"; reconf_dtweaks; reconf_dgovernor
	fi
else
	echo ":: $TWEAKS_OUTPUT_CREATING_CONFIG_DAILY"; reconf_dtweaks; reconf_dgovernor
fi
if [[ -f $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
	if [[ -s $STUDIO_SYSTEM_TWEAKS_PATH ]]; then
		echo ":: $TWEAKS_OUTPUT_CONFIGURED_STUDIO"
	else
		echo ":: $DST_OUTPUT_CONFIG_EMPTY"; echo " -> $TWEAKS_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_stweaks; reconf_sgovernor
	fi
else
	echo ":: $TWEAKS_OUTPUT_CREATING_CONFIG_STUDIO"; reconf_stweaks; reconf_sgovernor
fi
}

create_xdg_list() {
cp /etc/xdg/autostart/* /tmp/
MOVE_ITEMS=$(grep -wE 'Hidden' /tmp/*desktop | awk -F ':' '{print $1}')
	for i in $MOVE_ITEMS; do
	mv $i $HOME/.config/autostart/
	done 1> /dev/null
CONF_ITEMS=$(ls /tmp/*desktop)
	for i in $CONF_ITEMS; do
	echo "Hidden=false" | tee -a $i
	mv $i $HOME/.config/autostart/
	done 1> /dev/null
}

reconf_ddesktop() {
	# Create autostart items from /etc/xdg/autostart
	create_xdg_list
	# Session and autostart
	grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | sed 's/:Hidden//' > "$DAILY_CONFIG_DESKTOP_PATH.new"
	# Invert result for zenity --checklist
	TRUE_ITEM=$(grep -w 'true' "$DAILY_CONFIG_DESKTOP_PATH.new")
		for i in $TRUE_ITEM; do
			sed -i "/$i/ s/true/false/" "$DAILY_CONFIG_DESKTOP_PATH.new"
		done 1> /dev/null
	FALSE_ITEM=$(grep -w 'false' "$DAILY_CONFIG_DESKTOP_PATH.new")
		for i in $FALSE_ITEM; do
			sed -i "/$i/ s/false/true/" "$DAILY_CONFIG_DESKTOP_PATH.new"
		done 1> /dev/null
	REMOVE_ITEM=$(diff -y "$DAILY_CONFIG_DESKTOP_PATH.new" $DAILY_CONFIG_DESKTOP_PATH | grep -n '>' | awk -F ':' '{print $1}')
	  if [[ -z $REMOVE_ITEM ]]; then
	    echo ""
	  else
	    for i in $REMOVE_ITEM; do
	      sed -i "$i d" $DAILY_CONFIG_DESKTOP_PATH
	    done 1> /dev/null
	  fi
	  diff -y "$DAILY_CONFIG_DESKTOP_PATH.new" $DAILY_CONFIG_DESKTOP_PATH | grep '<' | awk '{print $1}' >> $DAILY_CONFIG_DESKTOP_PATH
	  sort $DAILY_CONFIG_DESKTOP_PATH > "$DAILY_CONFIG_DESKTOP_PATH.new"
	  mv "$DAILY_CONFIG_DESKTOP_PATH.new" $DAILY_CONFIG_DESKTOP_PATH
}

reconf_sdesktop() {
# Create autostart items from /etc/xdg/autostart
create_xdg_list
# Session and autostart
grep -wE 'Hidden' $HOME/.config/autostart/*.desktop | awk -F '/' '{print $6}' | sed 's/:Hidden//' > "$STUDIO_CONFIG_DESKTOP_PATH.new"
# Invert result for zenity --checklist
TRUE_ITEM=$(grep -w 'true' "$STUDIO_CONFIG_DESKTOP_PATH.new")
	for i in $TRUE_ITEM; do
		sed -i "/$i/ s/true/false/" "$STUDIO_CONFIG_DESKTOP_PATH.new"
	done 1> /dev/null
FALSE_ITEM=$(grep -w 'false' "$STUDIO_CONFIG_DESKTOP_PATH.new")s
	for i in $FALSE_ITEM; do
		sed -i "/$i/ s/false/true/" "$STUDIO_CONFIG_DESKTOP_PATH.new"
	done 1> /dev/null
REMOVE_ITEM=$(diff -y "$STUDIO_CONFIG_DESKTOP_PATH.new" $STUDIO_CONFIG_DESKTOP_PATH | grep -n '>' | awk -F ':' '{print $1}')
  if [[ -z $REMOVE_ITEM ]]; then
    echo ""
  else
    for i in $REMOVE_ITEM; do
      sed -i "$i d" $STUDIO_CONFIG_DESKTOP_PATH
    done 1> /dev/null
  fi
  diff -y "$STUDIO_CONFIG_DESKTOP_PATH.new" $STUDIO_CONFIG_DESKTOP_PATH | grep '<' | awk '{print $1}' >> $STUDIO_CONFIG_DESKTOP_PATH
  sort $STUDIO_CONFIG_DESKTOP_PATH > "$STUDIO_CONFIG_DESKTOP_PATH.new"
  mv "$STUDIO_CONFIG_DESKTOP_PATH.new" $STUDIO_CONFIG_DESKTOP_PATH
}

reconf_dservices(){
echo -e "bluetooth.service=true\ncups.service=false\ntlp.service=false\ncpupower.service=false\nhpet.service=false\nrtc.service=false\nufw.service=true\nfirewalld.service=false\nlibvirtd.service=false" > $DAILY_CONFIG_SERVICE_PATH
}

reconf_sservices(){
echo -e "bluetooth.service=false\ncups.service=false\ntlp.service=false\ncpupower.service=false\nhpet.service=false\nrtc.service=false\nufw.service=false\nfirewalld.service=false\nlibvirtd.service=false" > $STUDIO_CONFIG_SERVICE_PATH
}

reconf_dtweaks() {
echo -e "audio_group=true\nrealtime_group=false\nswappiness=false\nmaximum_watches=false\nfrequency_scaling_tlp=false\nfrequency_scaling_cpu=false\nthreadirqs=false\nunlock_memory=false\nopen_files_limit=false\nprecision_event_timer=false" > $DAILY_SYSTEM_TWEAKS_PATH
}

reconf_stweaks() {
echo -e "audio_group=true\nrealtime_group=true\nswappiness=true\nmaximum_watches=true\nfrequency_scaling_tlp=false\nfrequency_scaling_cpu=false\nthreadirqs=true\nunlock_memory=true\nopen_files_limit=true\nprecision_event_timer=true" > $STUDIO_SYSTEM_TWEAKS_PATH
}

reconf_dgovernor() {
echo -e "gov_ondemand=false\ngov_powersave=false\ngov_userspace=false\ngov_performance=false\ngov_conservative=false\ngov_schedutil=true" > $DAILY_GOVERNOR_PATH
}

reconf_sgovernor() {
echo -e "gov_ondemand=false\ngov_powersave=false\ngov_userspace=false\ngov_performance=true\ngov_conservative=false\ngov_schedutil=false" > $STUDIO_GOVERNOR_PATH
}

error_dialog() {
zenity --error --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_SYNC_TEXT" \
--icon-name="recbox-workflow-settings" --window-icon="$ICON_PATH" --no-wrap
}

case "$1" in
	--settings | -S ) settings;;
	--sync | -s ) sync_configs;;
	-sS | -Ss | -ss | -SS ) sync_configs && settings || error_dialog;;
	--daily-submenu ) daily_submenu;;
	--studio-submenu ) studio_submenu;;
	--daily-desktop-submenu ) settings_daily_desktop_menu;;
	--studio-desktop-submenu ) settings_studio_desktop_menu;;
	--daily-services-submenu ) settings_daily_service_menu;;
	--studio-services-submenu ) settings_studio_service_menu;;
	--daily-tweaks-submenu ) settings_daily_tweaks_menu;;
	--studio-tweaks-submenu ) settings_studio_tweaks_menu;;
	--daily-governor-menu ) daily_governor_menu;;
	--studio-governor-menu ) studio_governor_menu;;
	--reconf-daily-desktop ) reconf_ddesktop;;
	--reconf-studio-desktop ) reconf_sdesktop;;
	--restart-configs | -r ) reconf_ddesktop; reconf_sdesktop; reconf_dservices; reconf_sservices; reconf_dtweaks; reconf_stweaks; reconf_dgovernor; reconf_sgovernor; exec rb-workflow-settings.sh -s;;
	--version | -v ) echo -e "\n    Version $VER\n";;
	*)
echo -e "
	Usage:\n
	rb-workflow.sh [ OPTION ]\n
	Options:\n
	--settings, -S				settings main menu
	--sync, -s				syncronize config files
	-sS, -Ss, -ss, -SS			syncronize config files
						and settings mein menu
	--daily-submenu				Daily submenu
	--studio-submenu			Studio submenu
	--daily-desktop-submenu			Daily desktop submenu
	--studio-desktop-submenu		Studio desktop submenu
	--daily-services-submenu		Daily service submenu
	--studio-services-submenu		Studio service submenu
	--daily-tweaks-submenu			Daily tweaks submenu
	--studio-tweaks-submenu			Studio tweaks menu
	--daily-governor-menu			Daily TLP governor submenu
	--studio-governor-menu		Studio TLP governor submenu
	--daily-cpupower-scaling-submenu	Daily CPU Power governor submenu
	--studio-cpupower-scaling-submenu	Studio CPU governor submenu
	--restart-configs, -r			reconfigure Daily desktop config
						reconfigure Studio desktop config
						reconfigure Daily services config
						reconfigure Studio services config
						reconfigure Daily tweaks config
						reconfigure Studio tweaks config
	--version, -v				Workflow Settings version
" >&2
exit 1;;
esac
exit 0
