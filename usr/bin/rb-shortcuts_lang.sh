#!/usr/bin/env bash

SYS_LANG=$(echo $LANG | cut -d '.' -f 1)
if [[ -z $(find /usr/share/xfce-shortcuts/translations/ -name $SYS_LANG*) ]]; then
	source /usr/share/xfce-shortcuts/translations/en_US.trans
else
	for TRANS in /usr/share/xfce-shortcuts/translations/$SYS_LANG*; do
		source $TRANS
	done
fi
