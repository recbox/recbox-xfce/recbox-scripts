#!/usr/bin/env bash

DAILY_CONFIG_DESKTOP_PATH="$HOME/.config/workflow-settings/daily-desktop"
STUDIO_CONFIG_DESKTOP_PATH="$HOME/.config/workflow-settings/studio-desktop"

config_daily_desktop() {
TRUE=$(grep -w 'true' $DAILY_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}')
FALSE=$(grep -w 'false' $DAILY_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}')
# Enable items in Autostart (Session and Startup)
for input in $TRUE; do
	sed -i '/Hidden/ s/true/false/' $HOME/.config/autostart/$input
done
# Disable items in Autostart (Session and Startup)
for input in $FALSE; do
	sed -i '/Hidden/ s/false/true/' $HOME/.config/autostart/$input
done
}

config_studio_desktop() {
TRUE=$(grep -w 'true' $STUDIO_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}')
FALSE=$(grep -w 'false' $STUDIO_CONFIG_DESKTOP_PATH | awk -F '=' '{print $1}')
# Enable items in Autostart (Session and Startup)
for input in $TRUE; do
	sed -i '/Hidden/ s/true/false/' $HOME/.config/autostart/$input
done
# Disable items in Autostart (Session and Startup)
for input in $FALSE; do
	sed -i '/Hidden/ s/false/true/' $HOME/.config/autostart/$input
done
}

case "$1" in
	--daily | -d ) config_daily_desktop;;
	--studio | -s ) config_studio_desktop;;
	*)
echo -e "
	Usage:\n
	rb-workflow-run.sh [ OPTION ]\n
	Options:\n
	--daily, -d
	--studio, -s
" >&2
exit 1;;
esac
exit 0
