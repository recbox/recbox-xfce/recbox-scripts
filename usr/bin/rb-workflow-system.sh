#!/usr/bin/env bash

source /usr/bin/rb-workflow-system_lang.sh

REALTIME_PRIVILEGES=$(which realtime-privileges 2> /dev/null)
HPET_SERVICE=$(find /etc/systemd/system/ -name hpet.service)
RTC_SERVICE=$(find /etc/systemd/system/ -name rtc.service)

BLUETOOTH_SERVICE_STATUS=$(systemctl status bluetooth.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
CUPS_SERVICE_STATUS=$(systemctl status cups.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
CPUPOWER_SERVICE_STATUS=$(systemctl status cpupower.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
TLP_SERVICE_STATUS=$(systemctl status tlp.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
HPET_SERVICE_STATUS=$(head -1 /proc/sys/dev/hpet/max-user-freq)
RTC_SERVICE_STATUS=$(head -1 /sys/class/rtc/rtc0/max_user_freq)
UFW_SERVICE_STATUS=$(systemctl status ufw.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
FIREWALLD_SERVICE_STATUS=$(systemctl status firewalld.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')
VIRTMANAGER_SERVICE_STATUS=$(systemctl status libvirtd.service 2>/dev/null | grep -w 'Active' | awk '{print $2}')

GOVERNOR_SYSTEM_SETTINGS=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)
FETCH_USER=$(grep -w 'USER' /tmp/workflow/user | awk -F '=' '{print $2}')

config_daily_system() {
DAILY_CONFIG_TWEAKS_PATH="/home/$FETCH_USER/.config/workflow-settings/daily-tweaks"
DAILY_CONFIG_SERVICE_PATH="/home/$FETCH_USER/.config/workflow-settings/daily-service"
GOVERNOR_USER_SETTINGS=$(grep -w 'true' /home/$FETCH_USER/.config/workflow-settings/daily-governor | awk '$0=$2' FS=_ RS='=')

DAILY_BLUETOOTH_SERVICE=$(grep -w 'bluetooth.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_CUPS_SERVICE=$(grep -w 'cups.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_CPUPOWER_SERVICE=$(grep -w 'cpupower.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_TLP_SERVICE=$(grep -w 'tlp.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_HPET_SERVICE=$(grep -w 'hpet.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_RTC_SERVICE=$(grep -w 'rtc.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_UFW_SERVICE=$(grep -w 'ufw.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_FIREWALLD_SERVICE=$(grep -w 'firewalld.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
DAILY_VIRTMANAGER_SERVICE=$(grep -w 'libvirtd.service' $DAILY_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')

################################################################################
############################### SYSTEM SERVICES ################################
################################################################################
echo -e "==> $BLUETOOTH_CONF_START\n  -> $LOOKING_FOR_BLUETOOTH"
	if [[ -z $BLUETOOTH_SERVICE_STATUS ]]; then
		echo -e "  -> $BLUEZ_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_BLUETOOTH_SERVICE == "true" ]]; then
				if [[ $BLUETOOTH_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $BLUETOOTH_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $BLUETOOTH_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $BLUETOOTH_INACTIVE\n  -> $ENABLING_BLUETOOTH"
					systemctl enable bluetooth.service
				fi
		elif [[ $DAILY_BLUETOOTH_SERVICE == "false" ]]; then
				if [[ $BLUETOOTH_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $BLUETOOTH_ACTIVE\n  -> $DISABLING_BLUETOOTH"
					systemctl disable bluetooth.service
				elif [[ $BLUETOOTH_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $BLUETOOTH_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
echo -e "==> $CUPS_CONF_START\n  -> $LOOKING_FOR_CUPS"
	if [[ -z $CUPS_SERVICE_STATUS ]]; then
		echo -e "  -> $CUPS_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_CUPS_SERVICE == "true" ]]; then
				if [[ $CUPS_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CUPS_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $CUPS_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CUPS_INACTIVE\n  -> $ENABLING_CUPS"
					systemctl enable cups.service
					systemctl enable cups.socket
					systemctl enable cups.path
				fi
		elif [[ $DAILY_CUPS_SERVICE == "false" ]]; then
				if [[ $CUPS_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CUPS_ACTIVE\n  -> $DISABLING_CUPS"
					systemctl disable cups.service
					systemctl disable cups.socket
					systemctl disable cups.path
				elif [[ $CUPS_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CUPS_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
if [[ $DAILY_CPUPOWER_SERVICE == "false" ]] && [[ $DAILY_TLP_SERVICE == "false" ]]; then
	echo -e "==> $NONE_CPUPOWER_TLP_SERVICE\n  -> $SKIPPING_PROCESS"
elif $DAILY_CPUPOWER_SERVICE == "true" ]] && [[ $DAILY_TLP_SERVICE == "false" ]]; then
	echo -e "==> $CPUPOWER_CONF_START\n  -> $LOOKING_FOR_CPUPOWER"
		if [[ -z $CPUPOWER_SERVICE_STATUS ]]; then
			echo -e "  -> $CPUPOWER_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $DAILY_CPUPOWER_SERVICE == "true" ]]; then
				if [[ $CPUPOWER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CPUPOWER_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $CPUPOWER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CPUPOWER_INACTIVE\n  -> $ENABLING_CPUPOWER"
					systemctl enable cpupower.service
				fi
			elif [[ $DAILY_CPUPOWER_SERVICE == "false" ]]; then
				if [[ $CPUPOWER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CPUPOWER_ACTIVE\n  -> $DISABLING_CPUPOWER"
					systemctl disable cpupower.service
				elif [[ $CPUPOWER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CPUPOWER_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
elif [[ $DAILY_CPUPOWER_SERVICE == "false" ]] && [[ $DAILY_TLP_SERVICE == "true" ]]; then
	echo -e "==> $TLP_CONF_START\n  -> $LOOKING_FOR_TLP"
		if [[ -z $TLP_SERVICE_STATUS ]]; then
			echo -e "  -> $TLP_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $DAILY_TLP_SERVICE == "true" ]]; then
				if [[ $TLP_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $TLP_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $TLP_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $TLP_INACTIVE\n  -> $ENABLING_TLP"
					systemctl enable tlp.service
				fi
			elif [[ $DAILY_TLP_SERVICE == "false" ]]; then
				if [[ $TLP_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $TLP_ACTIVE\n  -> $DISABLING_TLP"
					systemctl disable tlp.service
				elif [[ $TLP_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $TLP_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
fi
echo -e "==> $HPET_CONF_START\n  -> $LOOKING_FOR_HPET"
	if [[ -z $HPET_SERVICE ]]; then
		echo -e "  -> $HPET_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_HPET_SERVICE == "true" ]]; then
			if [[ $HPET_SERVICE_STATUS -ge "3072" ]]; then
				echo -e "  -> $HPET_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
			elif [[ $HPET_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $HPET_INACTIVE\n  -> $ENABLING_HPET"
				systemctl enable hpet.service
			fi
		elif [[ $DAILY_HPET_SERVICE == "false" ]]; then
			if [[ $HPET_SERVICE_STATUS -ge "3072" ]]; then
				echo -e "  -> $HPET_ACTIVE\n  -> $DISABLING_HPET"
				systemctl disable hpet.service
			elif [[ $HPET_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $HPET_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	fi
echo -e "==> $RTC_CONF_START\n  -> $LOOKING_FOR_RTC"
	if [[ -z $RTC_SERVICE ]]; then
		echo -e "  -> $RTC_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_RTC_SERVICE == "true" ]]; then
			if [[ $RTC_SERVICE_STATUS -gt "3072" ]]; then
				echo -e "  -> $RTC_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
			elif [[ $RTC_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $RTC_INACTIVE\n  -> $ENABLING_RTC"
				systemctl enable rtc.service
			fi
		elif [[ $DAILY_RTC_SERVICE == "false" ]]; then
			if [[ $RTC_SERVICE_STATUS -ge "3072" ]]; then
				echo -e "  -> $RTC_ACTIVE\n  -> $DISABLING_RTC"
				systemctl disable rtc.service
			elif [[ $RTC_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $RTC_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	fi
if [[ $DAILY_UFW_SERVICE == "false" ]] && [[ $DAILY_FIREWALLD_SERVICE == "false" ]]; then
	echo -e "==> $UFW_CONF_START\n  -> $LOOKING_FOR_UFW"
		if [[ -z $UFW_SERVICE_STATUS ]]; then
			echo -e "  -> $UFW_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $UFW_SERVICE_STATUS == "active" ]]; then
				echo -e "  -> $UFW_ACTIVE\n  -> $DISABLING_UFW"
				systemctl disable ufw.service
			elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
				echo -e "  -> $UFW_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	echo -e "==> $FIREWALLD_CONF_START\n  -> $LOOKING_FOR_FIREWALLD"
		if [[ -z $FIREWALLD_SERVICE_STATUS ]]; then
			echo -e "  -> $FIREWALLD_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
				echo -e "  -> $FIREWALLD_ACTIVE\n  -> $DISABLING_FIREWALLD"
				systemctl disable firewalld.service
			elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
				echo -e "  -> $FIREWALLD_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
elif [[ $DAILY_UFW_SERVICE == "true" ]] && [[ $DAILY_FIREWALLD_SERVICE == "false" ]]; then
	echo -e "==> $UFW_CONF_START\n  -> $LOOKING_FOR_UFW"
		if [[ -z $UFW_SERVICE_STATUS ]]; then
			echo -e "  -> $UFW_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $DAILY_UFW_SERVICE == "true" ]]; then
				if [[ $UFW_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $UFW_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $UFW_INACTIVE\n  -> $ENABLING_UFW"
					systemctl enable ufw.service
				fi
			elif [[ $DAILY_UFW_SERVICE == "false" ]]; then
				if [[ $UFW_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $UFW_ACTIVE\n  -> $DISABLING_UFW"
					systemctl disable ufw.service
				elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $UFW_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
elif [[ $DAILY_UFW_SERVICE == "false" ]] && [[ $DAILY_FIREWALLD_SERVICE == "true" ]]; then
	echo -e "==> $FIREWALLD_CONF_START\n  -> $LOOKING_FOR_FIREWALLD"
		if [[ -z $FIREWALLD_SERVICE_STATUS ]]; then
			echo -e "  -> $FIREWALLD_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $DAILY_FIREWALLD_SERVICE == "true" ]]; then
				if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $FIREWALLD_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $FIREWALLD_INACTIVE\n  -> $ENABLING_FIREWALLD"
					systemctl enable firewalld.service
				fi
			elif [[ $DAILY_FIREWALLD_SERVICE == "false" ]]; then
				if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $FIREWALLD_ACTIVE\n  -> $DISABLING_FIREWALLD"
					systemctl disable firewalld.service
				elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $FIREWALLD_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
fi
echo -e "==> $VIRTMANAGER_CONF_START\n  -> $LOOKING_FOR_VIRTMANAGER"
	if [[ -z $VIRTMANAGER_SERVICE_STATUS ]]; then
		echo -e "  -> $VIRTMANAGER_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_VIRTMANAGER_SERVICE == "true" ]]; then
				if [[ $VIRTMANAGER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $VIRTMANAGER_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $VIRTMANAGER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $VIRTMANAGER_INACTIVE\n  -> $ENABLING_VIRTMANAGER"
					systemctl enable libvirtd.service
				fi
		elif [[ $DAILY_VIRTMANAGER_SERVICE == "false" ]]; then
				if [[ $VIRTMANAGER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $VIRTMANAGER_ACTIVE\n  -> $DISABLING_VIRTMANAGER"
					systemctl disable libvirtd.service
				elif [[ $VIRTMANAGER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $VIRTMANAGER_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
echo "==> $SERVICES_CONFIGURATION_COMPLETE"

DAILY_AUDIO_GROUP=$(grep -w 'audio_group' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_REALTIME_GROUP=$(grep -w 'realtime_group' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_SWAPPINESS_CONF=$(grep -w 'swappiness' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_MAX_WATCHES_CONF=$(grep -w 'maximum_watches' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_FREQUENCY_SCALING_TLP=$(grep -w 'frequency_scaling_tlp' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_FREQUENCY_SCALING_CPUPOWER=$(grep -w 'frequency_scaling_cpu' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_THREADIRQS=$(grep -w 'threadirqs' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_MEMORY_UNLOCK_CONF=$(grep -w 'unlock_memory' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_OPEN_FILE_LIMIT_CONF=$(grep -w 'open_files_limit' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
DAILY_PRECISION_EVENT_TIMER_CONF=$(grep -w 'precision_event_timer' $DAILY_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')

################################################################################
################################ SYSTEM TWEAKS #################################
################################################################################
echo -e "==> $SYSTEM_TWEAKS\n  -> $AUDIO_GROUP_CONF"
	if [[ $DAILY_AUDIO_GROUP == "true" ]]; then
		echo "  -> $ADD_AUDIO_GROUP"
		if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
			usermod -a -G audio $FETCH_USER
		else
			echo -e "  -> $ALREADY_AUDIO_GROUP\n  -> $SKIPPING_PROCESS"
		fi
	elif [[ $DAILY_AUDIO_GROUP == "false" ]]; then
		echo "  -> $REMOVE_AUDIO_GROUP"
		if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
			echo -e "  -> $ALREADY_REMOVED_AUDIO_GROUP\n  -> $SKIPPING_PROCESS"
		else
			gpasswd -d $FETCH_USER audio
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo "==> $REALTIME_GROUP_CONF"
	if [[ -z $(find /boot/vmli* | grep 'rt') ]]; then
		echo -e "  -> $REALTIME_KERNEL_CHECK\n  -> $SKIPPING_PROCESS"
	elif [[ -z $REALTIME_PRIVILEGES ]]; then
		echo -e "  -> $REALTIME_GROUP_CHECK\n  -> $SKIPPING_PROCESS"
	else
		if [[ $DAILY_REALTIME_GROUP == "true" ]]; then
			echo "  -> $ADD_REALTIME_GROUP"
			if [[ -z $(groups $FETCH_USER | grep -w 'realtime') ]]; then
				usermod -a -G realtime $FETCH_USER
			else
				echo -e "  -> $ALREADY_REALTIME_GROUP\n  -> $SKIPPING_PROCESS"
			fi
		elif [[ $DAILY_REALTIME_GROUP == "false" ]]; then
			echo "  -> $REMOVE_REALTIME_GROUP"
			if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
				echo -e "  -> $ALREADY_REMOVED_REALTIME_GROUP\n  -> $SKIPPING_PROCESS"
			else
				gpasswd -d $FETCH_USER realtime
			fi
		else
			echo "  -> $ERROR_BOOLEN"
		fi
	fi
echo -e "==> $SWAPPINESS_CONF\n  -> $LOOKING_FOR_SYSCTL_CONF" #cat /proc/sys/vm/swappiness
	if [[ $DAILY_SWAPPINESS_CONF == "true" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 99-sysctl.conf) ]]; then
			echo -e "  -> $NO_SYSCTL_CONFIG\n  -> $CREATING_SYSCTL_CONFIG"
			echo -e "\nvm.swappiness=10\nvm.vfs_cache_pressure=50" | tee /etc/sysctl.d/99-sysctl.conf
		else
			echo -e "  -> $SYSCTL_CONF_EXIST\n  -> $CHECKING_SWAPPINESS_CONFIGURATION"
			if [[ -z $(grep -w 'vm.swappiness' /etc/sysctl.d/99-sysctl.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $VMSWAPPINESS_NOT_DEFINED\n  -> $ADDING_VMSWAPPINESS"
				echo -e "vm.swappiness=10" | tee -a /etc/sysctl.d/99-sysctl.conf
			fi
			if [[ -z $(grep -w 'vm.vfs_cache_pressure' /etc/sysctl.d/99-sysctl.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $VFSCACHE_PRESSURE_NOT_DEFINED\n  -> $ADDING_VFSCACHE_PRESSURE"
				echo -e "vm.vfs_cache_pressure=50" | tee -a /etc/sysctl.d/99-sysctl.conf
			fi
		fi
	elif [[ $DAILY_SWAPPINESS_CONF == "false" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 99-sysctl.conf) ]]; then
			echo -e "  -> $NO_SYSCTL_CONFIG\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $SYSCTL_CONF_EXIST\n  -> $REMOVING_VMSWAPPINESS\n  -> $REMOVING_VFSCACHE_PRESSURE"
			sed -i "$(grep -wn 'vm.swappiness' /etc/sysctl.d/99-sysctl.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/99-sysctl.conf
			sed -i "$(grep -wn 'vm.vfs_cache_pressure' /etc/sysctl.d/99-sysctl.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/99-sysctl.conf
			echo "  -> $LOOKING_FOR_OTHER_SETTINGS"
			if [[ -s /etc/sysctl.d/99-sysctl.conf ]]; then
  			echo -e "  -> $CONFIG_NOT_EMPTY\n  -> $SKIPPING_PROCESS"
			else
  			echo -e "  -> $REMOVING_EMPTY_FILE"
				rm /etc/sysctl.d/99-sysctl.conf
			fi
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $MAX_WATCHES_CONF\n  -> $LOOKING_FOR_MAX_USER_WATCHES_CONF"
	if [[ $DAILY_MAX_WATCHES_CONF == "true" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 90-max_user_watches.conf) ]]; then
			echo -e "  -> $NO_MAX_USER_WATCHES_CONFIG\n  -> $CREATING_MAX_USER_WATCHES_CONFIG"
			echo -e "fs.inotify.max_user_watches = 600000" | tee /etc/sysctl.d/90-max_user_watches.conf
		else
			echo -e "  -> $MAX_WATCHES_CONF_EXIST\n  -> $CHECKING_MAX_WATCHES_CONFIGURATION"
			if [[ -z $(grep -w 'fs.inotify.max_user_watches' /etc/sysctl.d/90-max_user_watches.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $MAXIMUM_WATCHES_NOT_DEFINED\n  -> $ADDING_MAX_USER_WATCHES"
				echo -e "fs.inotify.max_user_watches = 600000" | tee -a /etc/sysctl.d/90-max_user_watches.conf
			fi
		fi
	elif [[ $DAILY_MAX_WATCHES_CONF == "false" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 90-max_user_watches.conf) ]]; then
			echo -e "  -> $NO_MAX_USER_WATCHES_CONFIG\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $MAX_WATCHES_CONF_EXIST\n  -> $REMOVING_MAX_USER_WATCHES"
			sed -i "$(grep -wn 'fs.inotify.max_user_watches' /etc/sysctl.d/90-max_user_watches.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/90-max_user_watches.conf
			echo "  -> $LOOKING_FOR_OTHER_SETTINGS"
			if [[ -s /etc/sysctl.d/90-max_user_watches.conf ]]; then
  			echo -e "  -> $CONFIG_NOT_EMPTY\n  -> $SKIPPING_PROCESS"
			else
  			echo -e "  -> $REMOVING_EMPTY_FILE"
				rm /etc/sysctl.d/90-max_user_watches.conf
			fi
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
if [[ $DAILY_FREQUENCY_SCALING_CPUPOWER == "false" ]] && [[ $DAILY_FREQUENCY_SCALING_TLP == "false" ]]; then
	echo -e "==> $NONE_CPUPOWER_TLP_GOVERNOR\n  -> $SKIPPING_PROCESS"
elif [[ $DAILY_FREQUENCY_SCALING_CPUPOWER == "true" ]] && [[ $DAILY_FREQUENCY_SCALING_TLP == "false" ]]; then
	echo "==> $FREQUENCY_SCALING_CPUPOWER"
		if [[ $DAILY_FREQUENCY_SCALING_CPUPOWER == "true" ]]; then
			echo "  -> $SETTING_CPU_SCALING"
			if [[ $GOVERNOR_USER_SETTINGS == $GOVERNOR_SYSTEM_SETTINGS ]]; then
			  echo -e "  -> $GOVERNOR_ALREADY_SET\n  -> $SKIPPING_PROCESS"
			else
			  echo "  -> $SETTING_UP_GOVERNOR"
				cpupower frequency-set -g $GOVERNOR_USER_SETTINGS
			fi
		elif [[ $DAILY_FREQUENCY_SCALING_CPUPOWER == "false" ]]; then
			echo -e "  -> $FREQUENCY_SCALLING_CPUPOWER_DISABLED\n  -> $STUDIO_SETTINGS_USED"
		else
			echo "  -> $ERROR_BOOLEN"
		fi
elif [[ $DAILY_FREQUENCY_SCALING_CPUPOWER == "false" ]] && [[ $DAILY_FREQUENCY_SCALING_TLP == "true" ]]; then
	echo "==> $FREQUENCY_SCALING_TLP"
		if [[ $DAILY_FREQUENCY_SCALING_TLP == "true" ]]; then
			echo "  -> $SETTING_CPU_SCALING"
			if [[ $GOVERNOR_USER_SETTINGS == $GOVERNOR_SYSTEM_SETTINGS ]]; then
			  echo -e "  -> $GOVERNOR_ALREADY_SET\n  -> $SKIPPING_PROCESS"
			else
			  echo "  -> $SETTING_UP_GOVERNOR"
				sed -i "s/$(grep -w 'CPU_SCALING_GOVERNOR_ON_AC' /etc/tlp.conf)/CPU_SCALING_GOVERNOR_ON_AC=$GOVERNOR_USER_SETTINGS/" /etc/tlp.conf
			fi
		elif [[ $DAILY_FREQUENCY_SCALING_TLP == "false" ]]; then
			echo -e "  -> $FREQUENCY_SCALLING_TLP_DISABLED\n  -> $STUDIO_SETTINGS_USED"
		else
			echo "  -> $ERROR_BOOLEN"
		fi
fi
echo -e "==> $THREADIRQS_CONF\n  -> $LOOKING_FOR_THREADIRQS_CONF"
	if [[ -z $(find /etc/default/ -name grub.old) ]]; then
		cp /etc/default/grub /etc/default/grub.old
	fi
	if [[ $DAILY_THREADIRQS == "true" ]]; then
		if [[ -z $(grep -wE '^GRUB_CMDLINE_LINUX_DEFAULT' /etc/default/grub | grep -w 'threadirqs') ]]; then
			echo -e "  -> $NO_THREADIRQS_PARAMETER\n  -> $ADDING_THREADIRQS_PARAMETER"
			sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAUL/ s/"$/ threadirqs"/' /etc/default/grub
		else
			echo -e "  -> $THREADIRQS_ALREADY_SET\n  -> $SKIPPING_PROCESS"
		fi
	elif [[ $DAILY_THREADIRQS == "false" ]]; then
		if [[ -z $(grep -wE '^GRUB_CMDLINE_LINUX_DEFAULT' /etc/default/grub | grep -w 'threadirqs') ]]; then
			echo -e "  -> $NO_THREADIRQS_PARAMETER\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $THREADIRQS_ALREADY_SET\n  -> $REMOVING_THREADIRQS_PARAMETER"
			sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAUL/ s/threadirqs//' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/  / /' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/" /"/' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/ "/"/' /etc/default/grub
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $MEMORY_UNLOCK_CONF\n  -> $LOOKING_FOR_REALTIME_PRIVILEGES_CONF"
	if [[ $DAILY_MEMORY_UNLOCK_CONF == "true" ]]; then
		if [[ -z $(find /etc/security/limits.d/ -name 99-realtime-privileges.conf) ]]; then
			echo -e "  -> $NO_REALTIME_PRIVILEGES_CONF\n  -> $CREATING_REALTIME_PRIVILEGES_CONF"
			echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
		else
			echo -e "  -> $REALTIME_PRIVILEGES_CONF_EXIST\n  -> $CHECKING_REALTIME_PRIVILEGES_CONF"
			if [[ -z $(grep "@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@realtime - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@audio - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
		fi
	elif [[ $DAILY_MEMORY_UNLOCK_CONF == "false" ]]; then
		if [[ -z $(find /etc/security/limits.d/ -name 99-realtime-privileges.conf) ]]; then
			echo -e "  -> $NO_REALTIME_PRIVILEGES_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $REALTIME_PRIVILEGES_CONF_EXIST\n  -> $REMOVING_REALTIME_PRIVILEGES_CONF"
			rm /etc/security/limits.d/99-realtime-privileges.conf
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $OPEN_FILE_LIMIT_CONF\n  -> $CHECKING_OPEN_FILE_LIMIT_CONF"
	if [[ -z $(find /etc/security/ -name limits.conf.old) ]]; then
		cp /etc/security/limits.conf /etc/security/limits.conf.old
	fi
	if [[ $DAILY_OPEN_FILE_LIMIT_CONF == "true" ]]; then
		if [[ -z $(grep -E "^# Open file limit increase" /etc/security/limits.conf) ]]; then
			echo -e "  -> $NO_OPEN_FILE_LIMIT_CONF\n  -> $ADDING_OPEN_FILE_LIMIT_CONF"
			echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
		else
			echo -e "  -> $OPEN_FILE_LIMIT_CONF_EXIST\n  -> $LOOKING_FOR_OPEN_FILE_LIMIT_CONF"
			if [[ -z $(grep -E "^*         hard    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^*         soft    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^root      hard    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^root      soft    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
		fi
	elif [[ $DAILY_OPEN_FILE_LIMIT_CONF == "false" ]]; then
		if [[ -z $(grep -E "^# Open file limit increase" /etc/security/limits.conf) ]]; then
			echo -e "  -> $NO_OPEN_FILE_LIMIT_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $OPEN_FILE_LIMIT_CONF_EXIST\n  -> $REMOVING_OPEN_FILE_LIMIT_CONF"
			sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $PRECISION_EVENT_TIMER_CONF\n  -> $LOOKING_FOR_PRECISION_EVENT_TIMER_CONF"
	if [[ $DAILY_PRECISION_EVENT_TIMER_CONF == "true"  ]]; then
		if [[ -z $(find /etc/udev/rules.d/ -name 40-timer-permissions.rules) ]]; then
			echo -e "  -> $NO_PRECISION_EVENT_TIMER_CONF\n  -> $CREATING_PRECISION_EVENT_TIMER_CONF"
			echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
		else
			echo -e "  -> $PRECISION_EVENT_TIMER_CONF_EXIST\n  -> $CHECKING_PRECISION_EVENT_TIMER_CONF"
			if [[ -z $(grep -E "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^KERNEL==\"hpet\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
		fi
	elif [[ $DAILY_PRECISION_EVENT_TIMER_CONF == "false" ]]; then
		if [[ -z $(find /etc/udev/rules.d/ -name 40-timer-permissions.rules) ]]; then
			echo -e "  -> $NO_PRECISION_EVENT_TIMER_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $PRECISION_EVENT_TIMER_CONF_EXIST\n  -> $REMOVING_PRECISION_EVENT_TIMER_CONF"
			rm /etc/udev/rules.d/40-timer-permissions.rules
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $SYSTEM_TWEAKS_CONFIGURATION_COMPLETE\n==> $GRUB_UPDATE"
	update-grub
}

config_studio_system() {
STUDIO_CONFIG_TWEAKS_PATH="/home/$FETCH_USER/.config/workflow-settings/studio-tweaks"
STUDIO_CONFIG_SERVICE_PATH="/home/$FETCH_USER/.config/workflow-settings/studio-service"
GOVERNOR_USER_SETTINGS=$(grep -w 'true' /home/$FETCH_USER/.config/workflow-settings/studio-governor | awk '$0=$2' FS=_ RS='=')

STUDIO_BLUETOOTH_SERVICE=$(grep -w 'bluetooth.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_CUPS_SERVICE=$(grep -w 'cups.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_CPUPOWER_SERVICE=$(grep -w 'cpupower.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_TLP_SERVICE=$(grep -w 'tlp.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_HPET_SERVICE=$(grep -w 'hpet.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_RTC_SERVICE=$(grep -w 'rtc.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_UFW_SERVICE=$(grep -w 'ufw.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_FIREWALLD_SERVICE=$(grep -w 'firewalld.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')
STUDIO_VIRTMANAGER_SERVICE=$(grep -w 'libvirtd.service' $STUDIO_CONFIG_SERVICE_PATH | awk -F '=' '{print $2}')

################################################################################
############################### SYSTEM SERVICES ################################
################################################################################
echo -e "==> $BLUETOOTH_CONF_START\n  -> $LOOKING_FOR_BLUETOOTH"
	if [[ -z $BLUETOOTH_SERVICE_STATUS ]]; then
		echo -e "  -> $BLUEZ_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_BLUETOOTH_SERVICE == "true" ]]; then
				if [[ $BLUETOOTH_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $BLUETOOTH_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $BLUETOOTH_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $BLUETOOTH_INACTIVE\n  -> $ENABLING_BLUETOOTH"
					systemctl enable bluetooth.service
				fi
		elif [[ $STUDIO_BLUETOOTH_SERVICE == "false" ]]; then
				if [[ $BLUETOOTH_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $BLUETOOTH_ACTIVE\n  -> $DISABLING_BLUETOOTH"
					systemctl disable bluetooth.service
				elif [[ $BLUETOOTH_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $BLUETOOTH_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
echo -e "==> $CUPS_CONF_START\n  -> $LOOKING_FOR_CUPS"
	if [[ -z $CUPS_SERVICE_STATUS ]]; then
		echo -e "  -> $CUPS_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_CUPS_SERVICE == "true" ]]; then
				if [[ $CUPS_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CUPS_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $CUPS_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CUPS_INACTIVE\n  -> $ENABLING_CUPS"
					systemctl enable cups.service
					systemctl enable cups.socket
					systemctl enable cups.path
				fi
		elif [[ $STUDIO_CUPS_SERVICE == "false" ]]; then
				if [[ $CUPS_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CUPS_ACTIVE\n  -> $DISABLING_CUPS"
					systemctl disable cups.service
					systemctl disable cups.socket
					systemctl disable cups.path
				elif [[ $CUPS_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CUPS_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
if [[ $STUDIO_CPUPOWER_SERVICE == "false" ]] && [[ $STUDIO_TLP_SERVICE == "false" ]]; then
	echo -e "==> $NONE_CPUPOWER_TLP_SERVICE\n  -> $SKIPPING_PROCESS"
elif [[ $STUDIO_CPUPOWER_SERVICE == "true" ]] && [[ $STUDIO_TLP_SERVICE == "false" ]]; then
	echo -e "==> $CPUPOWER_CONF_START\n  -> $LOOKING_FOR_CPUPOWER"
		if [[ -z $CPUPOWER_SERVICE_STATUS ]]; then
			echo -e "  -> $CPUPOWER_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $STUDIO_CPUPOWER_SERVICE == "true" ]]; then
				if [[ $CPUPOWER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CPUPOWER_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $CPUPOWER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CPUPOWER_INACTIVE\n  -> $ENABLING_CPUPOWER"
					systemctl enable cpupower.service
				fi
			elif [[ $STUDIO_CPUPOWER_SERVICE == "false" ]]; then
				if [[ $CPUPOWER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $CPUPOWER_ACTIVE\n  -> $DISABLING_CPUPOWER"
					systemctl disable cpupower.service
				elif [[ $CPUPOWER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $CPUPOWER_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
elif [[ $STUDIO_CPUPOWER_SERVICE == "false" ]] && [[ $STUDIO_TLP_SERVICE == "true" ]]; then
	echo -e "==> $TLP_CONF_START\n  -> $LOOKING_FOR_TLP"
		if [[ -z $TLP_SERVICE_STATUS ]]; then
			echo -e "  -> $TLP_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $STUDIO_TLP_SERVICE == "true" ]]; then
				if [[ $TLP_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $TLP_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $TLP_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $TLP_INACTIVE\n  -> $ENABLING_TLP"
					systemctl enable tlp.service
				fi
			elif [[ $STUDIO_TLP_SERVICE == "false" ]]; then
				if [[ $TLP_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $TLP_ACTIVE\n  -> $DISABLING_TLP"
					systemctl disable tlp.service
				elif [[ $TLP_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $TLP_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
fi
echo -e "==> $HPET_CONF_START\n  -> $LOOKING_FOR_HPET"
	if [[ -z $HPET_SERVICE ]]; then
		echo -e "  -> $HPET_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_HPET_SERVICE == "true" ]]; then
			if [[ $HPET_SERVICE_STATUS -gt "3072" ]]; then
				echo -e "  -> $HPET_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
			elif [[ $HPET_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $HPET_INACTIVE\n  -> $ENABLING_HPET"
				systemctl enable hpet.service
			fi
		elif [[ $STUDIO_HPET_SERVICE == "false" ]]; then
			if [[ $HPET_SERVICE_STATUS -ge "3072" ]]; then
				echo -e "  -> $HPET_ACTIVE\n  -> $DISABLING_HPET"
				systemctl disable hpet.service
			elif [[ $HPET_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $HPET_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	fi
echo -e "==> $RTC_CONF_START\n  -> $LOOKING_FOR_RTC"
	if [[ -z $RTC_SERVICE ]]; then
		echo -e "  -> $RTC_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_RTC_SERVICE == "true" ]]; then
			if [[ $RTC_SERVICE_STATUS -gt "3072" ]]; then
				echo -e "  -> $RTC_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
			elif [[ $RTC_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $RTC_INACTIVE\n  -> $ENABLING_RTC"
				systemctl enable rtc.service
			fi
		elif [[ $STUDIO_RTC_SERVICE == "false" ]]; then
			if [[ $RTC_SERVICE_STATUS -ge "3072" ]]; then
				echo -e "  -> $RTC_ACTIVE\n  -> $DISABLING_RTC"
				systemctl disable rtc.service
			elif [[ $RTC_SERVICE_STATUS -lt "3072" ]]; then
				echo -e "  -> $RTC_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	fi
if [[ $STUDIO_UFW_SERVICE == "false" ]] && [[ $STUDIO_FIREWALLD_SERVICE == "false" ]]; then
	echo -e "==> $UFW_CONF_START\n  -> $LOOKING_FOR_UFW"
		if [[ -z $UFW_SERVICE_STATUS ]]; then
			echo -e "  -> $UFW_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $UFW_SERVICE_STATUS == "active" ]]; then
				echo -e "  -> $UFW_ACTIVE\n  -> $DISABLING_UFW"
				systemctl disable ufw.service
			elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
				echo -e "  -> $UFW_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
	echo -e "==> $FIREWALLD_CONF_START\n  -> $LOOKING_FOR_FIREWALLD"
		if [[ -z $FIREWALLD_SERVICE_STATUS ]]; then
			echo -e "  -> $FIREWALLD_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
				echo -e "  -> $FIREWALLD_ACTIVE\n  -> $DISABLING_FIREWALLD"
				systemctl disable firewalld.service
			elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
				echo -e "  -> $FIREWALLD_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
			fi
		fi
elif [[ $STUDIO_UFW_SERVICE == "true" ]] && [[ $STUDIO_FIREWALLD_SERVICE == "false" ]]; then
	echo -e "==> $UFW_CONF_START\n  -> $LOOKING_FOR_UFW"
		if [[ -z $UFW_SERVICE_STATUS ]]; then
			echo -e "  -> $UFW_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $STUDIO_UFW_SERVICE == "true" ]]; then
				if [[ $UFW_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $UFW_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $UFW_INACTIVE\n  -> $ENABLING_UFW"
					systemctl enable ufw.service
				fi
			elif [[ $STUDIO_UFW_SERVICE == "false" ]]; then
				if [[ $UFW_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $UFW_ACTIVE\n  -> $DISABLING_UFW"
					systemctl disable ufw.service
				elif [[ $UFW_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $UFW_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
elif [[ $STUDIO_UFW_SERVICE == "false" ]] && [[ $STUDIO_FIREWALLD_SERVICE == "true" ]]; then
	echo -e "==> $FIREWALLD_CONF_START\n  -> $LOOKING_FOR_FIREWALLD"
		if [[ -z $FIREWALLD_SERVICE_STATUS ]]; then
			echo -e "  -> $FIREWALLD_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
		else
			if [[ $STUDIO_FIREWALLD_SERVICE == "true" ]]; then
				if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $FIREWALLD_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $FIREWALLD_INACTIVE\n  -> $ENABLING_FIREWALLD"
					systemctl enable firewalld.service
				fi
			elif [[ $STUDIO_FIREWALLD_SERVICE == "false" ]]; then
				if [[ $FIREWALLD_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $FIREWALLD_ACTIVE\n  -> $DISABLING_FIREWALLD"
					systemctl disable firewalld.service
				elif [[ $FIREWALLD_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $FIREWALLD_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
			fi
		fi
fi
echo -e "==> $VIRTMANAGER_CONF_START\n  -> $LOOKING_FOR_VIRTMANAGER"
	if [[ -z $VIRTMANAGER_SERVICE_STATUS ]]; then
		echo -e "  -> $VIRTMANAGER_NOT_INSTALLED\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_VIRTMANAGER_SERVICE == "true" ]]; then
				if [[ $VIRTMANAGER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $VIRTMANAGER_ALREADY_ACTIVE\n  -> $SKIPPING_PROCESS"
				elif [[ $VIRTMANAGER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $VIRTMANAGER_INACTIVE\n  -> $ENABLING_VIRTMANAGER"
					systemctl enable libvirtd.service
				fi
		elif [[ $STUDIO_VIRTMANAGER_SERVICE == "false" ]]; then
				if [[ $VIRTMANAGER_SERVICE_STATUS == "active" ]]; then
					echo -e "  -> $VIRTMANAGER_ACTIVE\n  -> $DISABLING_VIRTMANAGER"
					systemctl disable libvirtd.service
				elif [[ $VIRTMANAGER_SERVICE_STATUS == "inactive" ]]; then
					echo -e "  -> $VIRTMANAGER_ALREADY_INACTIVE\n  -> $SKIPPING_PROCESS"
				fi
		fi
	fi
echo "==> $SERVICES_CONFIGURATION_COMPLETE"

STUDIO_AUDIO_GROUP=$(grep -w 'audio_group' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_REALTIME_GROUP=$(grep -w 'realtime_group' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_SWAPPINESS_CONF=$(grep -w 'swappiness' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_MAX_WATCHES_CONF=$(grep -w 'maximum_watches' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_FREQUENCY_SCALING_TLP=$(grep -w 'frequency_scaling_tlp' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_FREQUENCY_SCALING_CPUPOWER=$(grep -w 'frequency_scaling_cpu' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_THREADIRQS=$(grep -w 'threadirqs' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_MEMORY_UNLOCK_CONF=$(grep -w 'unlock_memory' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_OPEN_FILE_LIMIT_CONF=$(grep -w 'open_files_limit' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')
STUDIO_PRECISION_EVENT_TIMER_CONF=$(grep -w 'precision_event_timer' $STUDIO_CONFIG_TWEAKS_PATH | awk -F '=' '{print $2}')

################################################################################
################################ SYSTEM TWEAKS #################################
################################################################################
echo -e ":: $SYSTEM_TWEAKS\n  -> $AUDIO_GROUP_CONF"
	if [[ $STUDIO_AUDIO_GROUP == "true" ]]; then
		echo "  -> $ADD_AUDIO_GROUP"
		if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
			usermod -a -G audio $FETCH_USER
		else
			echo -e "  -> $ALREADY_AUDIO_GROUP\n  -> $SKIPPING_PROCESS"
		fi
	elif [[ $STUDIO_AUDIO_GROUP == "false" ]]; then
		echo "  -> $REMOVE_AUDIO_GROUP"
		if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
			echo -e "  -> $ALREADY_REMOVED_AUDIO_GROUP\n  -> $SKIPPING_PROCESS"
		else
			gpasswd -d $FETCH_USER audio
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo "==> $REALTIME_GROUP_CONF"
	if [[ -z $(find /boot/vmli* | grep 'rt') ]]; then
		echo -e "  -> $REALTIME_KERNEL_CHECK\n  -> $SKIPPING_PROCESS"
	elif [[ -z $REALTIME_PRIVILEGES ]]; then
		echo -e "  -> $REALTIME_GROUP_CHECK\n  -> $SKIPPING_PROCESS"
	else
		if [[ $STUDIO_REALTIME_GROUP == "true" ]]; then
			echo "  -> $ADD_REALTIME_GROUP"
			if [[ -z $(groups $FETCH_USER | grep -w 'realtime') ]]; then
				usermod -a -G realtime $FETCH_USER
			else
				echo -e "  -> $ALREADY_REALTIME_GROUP\n  -> $SKIPPING_PROCESS"
			fi
		elif [[ $STUDIO_REALTIME_GROUP == "false" ]]; then
			echo "  -> $REMOVE_REALTIME_GROUP"
			if [[ -z $(groups $FETCH_USER | grep -w 'audio') ]]; then
				echo -e "  -> $ALREADY_REMOVED_REALTIME_GROUP\n  -> $SKIPPING_PROCESS"
			else
				gpasswd -d $FETCH_USER realtime
			fi
		else
			echo "  -> $ERROR_BOOLEN"
		fi
	fi
echo -e "==> $SWAPPINESS_CONF\n  -> $LOOKING_FOR_SYSCTL_CONF" #cat /proc/sys/vm/swappiness
	if [[ $STUDIO_SWAPPINESS_CONF == "true" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 99-sysctl.conf) ]]; then
			echo -e "  -> $NO_SYSCTL_CONFIG\n  -> $CREATING_SYSCTL_CONFIG"
			echo -e "\nvm.swappiness=10\nvm.vfs_cache_pressure=50" | tee /etc/sysctl.d/99-sysctl.conf
		else
			echo -e "  -> $SYSCTL_CONF_EXIST\n  -> $CHECKING_SWAPPINESS_CONFIGURATION"
			if [[ -z $(grep -w 'vm.swappiness' /etc/sysctl.d/99-sysctl.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $VMSWAPPINESS_NOT_DEFINED\n  -> $ADDING_VMSWAPPINESS"
				echo -e "vm.swappiness=10" | tee -a /etc/sysctl.d/99-sysctl.conf
			fi
			if [[ -z $(grep -w 'vm.vfs_cache_pressure' /etc/sysctl.d/99-sysctl.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $VFSCACHE_PRESSURE_NOT_DEFINED\n  -> $ADDING_VFSCACHE_PRESSURE"
				echo -e "vm.vfs_cache_pressure=50" | tee -a /etc/sysctl.d/99-sysctl.conf
			fi
		fi
	elif [[ $STUDIO_SWAPPINESS_CONF == "false" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 99-sysctl.conf) ]]; then
			echo -e "  -> $NO_SYSCTL_CONFIG\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $SYSCTL_CONF_EXIST\n  -> $REMOVING_VMSWAPPINESS\n  -> $REMOVING_VFSCACHE_PRESSURE"
			sed -i "$(grep -wn 'vm.swappiness' /etc/sysctl.d/99-sysctl.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/99-sysctl.conf
			sed -i "$(grep -wn 'vm.vfs_cache_pressure' /etc/sysctl.d/99-sysctl.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/99-sysctl.conf
			echo "  -> $LOOKING_FOR_OTHER_SETTINGS"
			if [[ -s /etc/sysctl.d/99-sysctl.conf ]]; then
  			echo -e "  -> $CONFIG_NOT_EMPTY\n  -> $SKIPPING_PROCESS"
			else
  			echo -e "  -> $REMOVING_EMPTY_FILE"
				rm /etc/sysctl.d/99-sysctl.conf
			fi
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $MAX_WATCHES_CONF\n  -> $LOOKING_FOR_MAX_USER_WATCHES_CONF" #sysctl fs.inotify
	if [[ $STUDIO_MAX_WATCHES_CONF == "true" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 90-max_user_watches.conf) ]]; then
			echo -e "  -> $NO_MAX_USER_WATCHES_CONFIG\n  -> $CREATING_MAX_USER_WATCHES_CONFIG"
			echo -e "fs.inotify.max_user_watches = 600000" | tee /etc/sysctl.d/90-max_user_watches.conf
		else
			echo -e "  -> $MAX_WATCHES_CONF_EXIST\n  -> $CHECKING_MAX_WATCHES_CONFIGURATION"
			if [[ -z $(grep -w 'fs.inotify.max_user_watches' /etc/sysctl.d/90-max_user_watches.conf | awk -F '=' '{print $1}') ]]; then
				echo -e "  -> $MAXIMUM_WATCHES_NOT_DEFINED\n  -> $ADDING_MAX_USER_WATCHES"
				echo -e "fs.inotify.max_user_watches = 600000" | tee -a /etc/sysctl.d/90-max_user_watches.conf
			fi
		fi
	elif [[ $STUDIO_MAX_WATCHES_CONF == "false" ]]; then
		if [[ -z $(find /etc/sysctl.d/ -name 90-max_user_watches.conf) ]]; then
			echo -e "  -> $NO_MAX_USER_WATCHES_CONFIG\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $MAX_WATCHES_CONF_EXIST\n  -> $REMOVING_MAX_USER_WATCHES"
			sed -i "$(grep -wn 'fs.inotify.max_user_watches' /etc/sysctl.d/90-max_user_watches.conf | awk -F ':' '{print $1}')d" /etc/sysctl.d/90-max_user_watches.conf
			echo "  -> $LOOKING_FOR_OTHER_SETTINGS"
			if [[ -s /etc/sysctl.d/90-max_user_watches.conf ]]; then
  			echo -e "  -> $CONFIG_NOT_EMPTY\n  -> $SKIPPING_PROCESS"
			else
  			echo -e "  -> $REMOVING_EMPTY_FILE"
				rm /etc/sysctl.d/90-max_user_watches.conf
			fi
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
if [[ $STUDIO_FREQUENCY_SCALING_CPUPOWER == "false" ]] && [[ $STUDIO_FREQUENCY_SCALING_TLP == "false" ]]; then
	echo -e "==> $NONE_CPUPOWER_TLP_GOVERNOR\n  -> $SKIPPING_PROCESS"
elif [[ $STUDIO_FREQUENCY_SCALING_CPUPOWER == "true" ]] && [[ $STUDIO_FREQUENCY_SCALING_TLP == "false" ]]; then
	echo "==> $FREQUENCY_SCALING_CPUPOWER"
		if [[ $STUDIO_FREQUENCY_SCALING_CPUPOWER == "true" ]]; then
			echo "  -> $SETTING_CPU_SCALING"
			if [[ $GOVERNOR_USER_SETTINGS == $GOVERNOR_SYSTEM_SETTINGS ]]; then
			  echo -e "  -> $GOVERNOR_ALREADY_SET\n  -> $SKIPPING_PROCESS"
			else
			  echo "  -> $SETTING_UP_GOVERNOR"
				cpupower frequency-set -g $GOVERNOR_USER_SETTINGS
			fi
		elif [[ $STUDIO_FREQUENCY_SCALING_CPUPOWER == "false" ]]; then
			echo -e "  -> $FREQUENCY_SCALLING_CPUPOWER_DISABLED\n  -> $DAILY_SETTINGS_USED"
		else
			echo "  -> $ERROR_BOOLEN"
		fi
elif [[ $STUDIO_FREQUENCY_SCALING_CPUPOWER == "false" ]] && [[ $STUDIO_FREQUENCY_SCALING_TLP == "true" ]]; then
	echo "==> $FREQUENCY_SCALING_TLP"
		if [[ $STUDIO_FREQUENCY_SCALING_TLP == "true" ]]; then
			echo "  -> $SETTING_CPU_SCALING"
			if [[ $GOVERNOR_USER_SETTINGS == $GOVERNOR_SYSTEM_SETTINGS ]]; then
			  echo -e "  -> $GOVERNOR_ALREADY_SET\n  -> $SKIPPING_PROCESS"
			else
			  echo "  -> $SETTING_UP_GOVERNOR"
				sed -i "s/$(grep -w 'CPU_SCALING_GOVERNOR_ON_AC' /etc/tlp.conf)/CPU_SCALING_GOVERNOR_ON_AC=$GOVERNOR_USER_SETTINGS/" /etc/tlp.conf
			fi
		elif [[ $STUDIO_FREQUENCY_SCALING_TLP == "false" ]]; then
			echo -e "  -> $FREQUENCY_SCALLING_TLP_DISABLED\n  -> $DAILY_SETTINGS_USED"
		else
			echo "  -> $ERROR_BOOLEN"
		fi
fi
echo -e "==> $THREADIRQS_CONF\n  -> $LOOKING_FOR_THREADIRQS_CONF"
	if [[ -z $(find /etc/default/ -name grub.old) ]]; then
		cp /etc/default/grub /etc/default/grub.old
	fi
	if [[ $STUDIO_THREADIRQS == "true" ]]; then
		if [[ -z $(grep -wE '^GRUB_CMDLINE_LINUX_DEFAULT' /etc/default/grub | grep -w 'threadirqs') ]]; then
			echo -e "  -> $NO_THREADIRQS_PARAMETER\n  -> $ADDING_THREADIRQS_PARAMETER"
			sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAUL/ s/"$/ threadirqs"/' /etc/default/grub
		else
			echo -e "  -> $THREADIRQS_ALREADY_SET\n  -> $SKIPPING_PROCESS"
		fi
	elif [[ $STUDIO_THREADIRQS == "false" ]]; then
		if [[ -z $(grep -wE '^GRUB_CMDLINE_LINUX_DEFAULT' /etc/default/grub | grep -w 'threadirqs') ]]; then
			echo -e "  -> $NO_THREADIRQS_PARAMETER\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $THREADIRQS_ALREADY_SET\n  -> $REMOVING_THREADIRQS_PARAMETER"
			sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAUL/ s/threadirqs//' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/  / /' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/" /"/' -e '/GRUB_CMDLINE_LINUX_DEFAUL/ s/ "/"/' /etc/default/grub
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $MEMORY_UNLOCK_CONF\n  -> $LOOKING_FOR_REALTIME_PRIVILEGES_CONF"
	if [[ $STUDIO_MEMORY_UNLOCK_CONF == "true" ]]; then
		if [[ -z $(find /etc/security/limits.d/ -name 99-realtime-privileges.conf) ]]; then
			echo -e "  -> $NO_REALTIME_PRIVILEGES_CONF\n  -> $CREATING_REALTIME_PRIVILEGES_CONF"
			echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
		else
			echo -e "  -> $REALTIME_PRIVILEGES_CONF_EXIST\n  -> $CHECKING_REALTIME_PRIVILEGES_CONF"
			if [[ -z $(grep "@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@realtime - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@audio - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
			if [[ -z $(grep "@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf) ]]; then
				sed -i "$(grep -En "^@realtime - rtprio 99" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}'),$(grep -En "^@audio - memlock unlimited" /etc/security/limits.d/99-realtime-privileges.conf | awk -F ':' '{print $1}')d" /etc/security/limits.d/99-realtime-privileges.conf
				echo -e "@realtime - rtprio 99\n@realtime - memlock unlimited\n@audio - rtprio 99\n@audio - memlock unlimited" | tee /etc/security/limits.d/99-realtime-privileges.conf
			fi
		fi
	elif [[ $STUDIO_MEMORY_UNLOCK_CONF == "false" ]]; then
		if [[ -z $(find /etc/security/limits.d/ -name 99-realtime-privileges.conf) ]]; then
			echo -e "  -> $NO_REALTIME_PRIVILEGES_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $REALTIME_PRIVILEGES_CONF_EXIST\n  -> $REMOVING_REALTIME_PRIVILEGES_CONF"
			rm /etc/security/limits.d/99-realtime-privileges.conf
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $OPEN_FILE_LIMIT_CONF\n  -> $CHECKING_OPEN_FILE_LIMIT_CONF"
	if [[ -z $(find /etc/security/ -name limits.conf.old) ]]; then
		cp /etc/security/limits.conf /etc/security/limits.conf.old
	fi
	if [[ $STUDIO_OPEN_FILE_LIMIT_CONF == "true" ]]; then
		if [[ -z $(grep -E "^# Open file limit increase" /etc/security/limits.conf) ]]; then
			echo -e "  -> $NO_OPEN_FILE_LIMIT_CONF\n  -> $ADDING_OPEN_FILE_LIMIT_CONF"
			echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
		else
			echo -e "  -> $OPEN_FILE_LIMIT_CONF_EXIST\n  -> $LOOKING_FOR_OPEN_FILE_LIMIT_CONF"
			if [[ -z $(grep -E "^*         hard    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^*         soft    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^root      hard    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
			if [[ -z $(grep -E "^root      soft    nofile      500000" /etc/security/limits.conf) ]]; then
				sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
				echo -e "# Open file limit increase\n\n*         hard    nofile      500000\n*         soft    nofile      500000\nroot      hard    nofile      500000\nroot      soft    nofile      500000" | tee -a /etc/security/limits.conf
			fi
		fi
	elif [[ $STUDIO_OPEN_FILE_LIMIT_CONF == "false" ]]; then
		if [[ -z $(grep -E "^# Open file limit increase" /etc/security/limits.conf) ]]; then
			echo -e "  -> $NO_OPEN_FILE_LIMIT_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $OPEN_FILE_LIMIT_CONF_EXIST\n  -> $REMOVING_OPEN_FILE_LIMIT_CONF"
			sed -i "$(grep -En "^# Open file limit increase" /etc/security/limits.conf | awk -F ':' '{print $1}'),$(grep -En "^root      soft    nofile      500000" /etc/security/limits.conf | awk -F ':' '{print $1}')d" /etc/security/limits.conf
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $PRECISION_EVENT_TIMER_CONF\n  -> $LOOKING_FOR_PRECISION_EVENT_TIMER_CONF"
	if [[ $STUDIO_PRECISION_EVENT_TIMER_CONF == "true"  ]]; then
		if [[ -z $(find /etc/udev/rules.d/ -name 40-timer-permissions.rules) ]]; then
			echo -e "  -> $NO_PRECISION_EVENT_TIMER_CONF\n  -> $CREATING_PRECISION_EVENT_TIMER_CONF"
			echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
		else
			echo -e "  -> $PRECISION_EVENT_TIMER_CONF_EXIST\n  -> $CHECKING_PRECISION_EVENT_TIMER_CONF"
			if [[ -z $(grep -E "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^KERNEL==\"hpet\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
			if [[ -z $(grep -E "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules) ]]; then
				sed -i "$(grep -En "^KERNEL==\"rtc0\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}'),$(grep -En "^GROUP=\"audio\"" /etc/udev/rules.d/40-timer-permissions.rules | awk -F ':' '{print $1}')d" /etc/udev/rules.d/40-timer-permissions.rules
				echo -e "KERNEL==\"rtc0\"\nGROUP=\"audio\"\nKERNEL==\"hpet\"\nGROUP=\"audio\"" | tee /etc/udev/rules.d/40-timer-permissions.rules
			fi
		fi
	elif [[ $STUDIO_PRECISION_EVENT_TIMER_CONF == "false" ]]; then
		if [[ -z $(find /etc/udev/rules.d/ -name 40-timer-permissions.rules) ]]; then
			echo -e "  -> $NO_PRECISION_EVENT_TIMER_CONF\n  -> $SKIPPING_PROCESS"
		else
			echo -e "  -> $PRECISION_EVENT_TIMER_CONF_EXIST\n  -> $REMOVING_PRECISION_EVENT_TIMER_CONF"
			rm /etc/udev/rules.d/40-timer-permissions.rules
		fi
	else
		echo "  -> $ERROR_BOOLEN"
	fi
echo -e "==> $SYSTEM_TWEAKS_CONFIGURATION_COMPLETE\n==> $GRUB_UPDATE"
	update-grub
}

case "$1" in
	--daily | -d ) config_daily_system;;
	--studio | -s ) config_studio_system;;
	*)
echo -e "
	Usage:\n
	rb-workflow-run.sh [ OPTION ]\n
	Options:\n
	--daily, -d
	--studio, -s
" >&2
exit 1;;
esac
exit 0
