#!/usr/bin/env bash

places() {
MENU_ITEMS=$(mktemp)
trap "rm -f ${MENU_ITEMS}" EXIT
	ls -d /run/media/$USER/* | sed -e "s/[[:space:]]/\\\ /g" > ${MENU_ITEMS}
	sed -i 's|/$||' ${MENU_ITEMS}
	sed -i 's|$|,exo-open /|' ${MENU_ITEMS}
	sed -i 's/r[^h]*$/&&/' ${MENU_ITEMS}
	sed -i "s|^/run/media/$USER/||" ${MENU_ITEMS}
	sed -i 's|,exo-open /$||' ${MENU_ITEMS}
	sed -i '1i ^sep(USB Device | Partition)' ${MENU_ITEMS}
	sed -i "\$a ^sep()" ${MENU_ITEMS}
	sed -i "\$a ^sep(Places)" ${MENU_ITEMS}
	echo 'Home,exo-open $HOME' >> ${MENU_ITEMS}
	ls -d $HOME/*/ | grep -v $USER/$USER >> ${MENU_ITEMS}
	sed -i 's|/$||' ${MENU_ITEMS}
	sed -i 's|$|,exo-open /|' ${MENU_ITEMS}
	sed -i 's/h[^h]*$/&&/' ${MENU_ITEMS}
	sed -i "s|^/home/$USER/||" ${MENU_ITEMS}
	sed -i 's/^//' ${MENU_ITEMS}
	sed -i 's|,exo-open /$||' ${MENU_ITEMS}
	sed -i 's/^sep()/^sep()/' ${MENU_ITEMS}
	sed -i 's/^sep(USB Device | Partition)/^sep(USB Device | Partition)/' ${MENU_ITEMS}
	sed -i 's/^sep(Places)/^sep(Places)/' ${MENU_ITEMS}
	sed -i "\$a ^sep()" ${MENU_ITEMS}
	sed -i "\$a     Back,rb-side-menu.sh -s" ${MENU_ITEMS}
	sed -i '1i ^sep(Places)' ${MENU_ITEMS}
jgmenu --config-file="$HOME/.config/side-menu/$(grep -w side_menurc $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file=${MENU_ITEMS}
}

backup_studio_session() {
MAIN_WINDOW_TITLE="RecBox - Studio Session Backup"
MAIN_WINDOW_TEXT="Backup your records and DAW configuration files with rsync. \
\n\n<b>Studio Session directory:</b>\n $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) \
\n\n<b>Backup Session directory:</b>\n $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) \
\n\n<b>DAW directory:</b>\n $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) \
\n\n<b>DAW backup directory:</b>\n $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2)"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
PICK_COLUMN_TITLE="        "
OPTION_COLUMN_TITLE="Option"
SEPARATOR=""
DESCRIPTION_COLUMN_TITLE="Description"
ICON_PATH=$(grep -w icon_path $HOME/.config/workflow-settings/config | cut -d '=' -f 2)
OPTION_1="Backup Studio Session"
OPTION_2="Backup DAW configs"
OPTION_3="Sync Studio Session"
OPTION_4="Sync DAW configs"
OPTION_5="Studio Session directory"
OPTION_6="Studio Session backup directory"
OPTION_7="DAW configs directory"
OPTION_8="DAW configs backup directory"
DESCRIPTION_1="Copy studio session files into preferred directory."
DESCRIPTION_2="Copy DAW config files into preferred directory."
DESCRIPTION_3="Copy studio session files into preferred directory."
DESCRIPTION_4="Copy DAW config files into preferred directory."
DESCRIPTION_5="Set studio session directory."
DESCRIPTION_6="Choose where studio session files will stored."
DESCRIPTION_7="Set DAW configs directory."
DESCRIPTION_8="Choose where DAW config files will stored."
ZEN_MAIN_MENU="zenity --list --radiolist --width=640 --height=530"
PROGRESS_BOX="zenity --progress --pulsate --no-cancel --width=360 --height=120"
CHECK_CONFIG=$(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 1)

	if [[ -z $CHECK_CONFIG ]]; then
		mkdir -p $HOME/.config/studio-session-backup; echo -e "studio_config_dir=\ndaw_config_dir=\nbackup_studio_session_dir=\nbackup_daw_config_dir=" > $HOME/.config/studio-session-backup/config
	fi

	MAIN_BOX=$($ZEN_MAIN_MENU --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--column="$PICK_COLUMN_TITLE" --column="$OPTION_COLUMN_TITLE" --column="$SEPARATOR" --column="$DESCRIPTION_COLUMN_TITLE" --column="$SEPARATOR" \
		--window-icon="$ICON_PATH" \
		TRUE "$OPTION_1" "$SPACE" "$DESCRIPTION_1" "$SPACE" \
		FALSE "$OPTION_2" "$SPACE" "$DESCRIPTION_2" "$SPACE" \
		FALSE "$OPTION_3" "$SPACE" "$DESCRIPTION_3" "$SPACE" \
		FALSE "$OPTION_4" "$SPACE" "$DESCRIPTION_4" "$SPACE" \
		FALSE "$OPTION_5" "$SPACE" "$DESCRIPTION_5" "$SPACE" \
		FALSE "$OPTION_6" "$SPACE" "$DESCRIPTION_6" "$SPACE" \
		FALSE "$OPTION_7" "$SPACE" "$DESCRIPTION_7" "$SPACE" \
		FALSE "$OPTION_8" "$SPACE" "$DESCRIPTION_8" "$SPACE")

			if [[ $MAIN_BOX == $OPTION_1 ]]; then
				if [[ -z $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your Studio Session directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				elif [[ -z $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your Studio Session backup directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				else
					rsync -rav $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) | $($PROGRESS_BOX --window-icon="$ICON_PATH" --title="$OPTION_1" --text="\n Copy $(echo $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))... to $(echo $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))...")
					exec rb-scripts-box.sh -s
				fi
			elif [[ $MAIN_BOX == $OPTION_2 ]]; then
				if [[ -z $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your DAW directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				elif [[ -z $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your DAW backup directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				else
					rsync -rav $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) | $($PROGRESS_BOX --window-icon="$ICON_PATH" --title="$OPTION_2" --text="\n Copy $(echo $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))... to $(echo $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))...")
					exec rb-scripts-box.sh -s
				fi
			elif [[ $MAIN_BOX == $OPTION_3 ]]; then
				if [[ -z $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your Studio Session backup directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				elif [[ -z $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your Studio Session directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				else
					rsync -rav $(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) | $($PROGRESS_BOX --window-icon="$ICON_PATH" --title="$OPTION_3" --text="\n Copy $(echo $(backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))... to $(echo $(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))...")
					exec rb-scripts-box.sh -s
				fi
			elif [[ $MAIN_BOX == $OPTION_4 ]]; then
				if [[ -z $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your DAW backup directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				elif [[ -z $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) ]]; then
					zenity --error --text="Your DAW directory is not set." --no-wrap
					exec rb-scripts-box.sh -s
				else
					rsync -rav $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2) | $($PROGRESS_BOX --window-icon="$ICON_PATH" --title="$OPTION_4" --text="\n Copy $(echo $(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))... to $(echo $(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | cut -d "/" -f 4-))...")
					exec rb-scripts-box.sh -s
				fi
			elif [[ $MAIN_BOX == $OPTION_5 ]]; then
				SEL_BOX=$(zenity --file-selection --title="Choose Studio Session directory" --directory --multiple | sed 's|/|\\/|g')
					sed -i "/studio_config_dir/ s/$(grep -w studio_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | sed 's|/|\\/|g')/$SEL_BOX/" $HOME/.config/studio-session-backup/config
					exec rb-scripts-box.sh -s
			elif [[ $MAIN_BOX == $OPTION_6 ]]; then
				SEL_BOX=$(zenity --file-selection --title="Choose backup directory for Studio Session" --directory --multiple | sed 's|/|\\/|g')
					sed -i "/backup_studio_session_dir/ s/$(grep -w backup_studio_session_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | sed 's|/|\\/|g')/$SEL_BOX/" $HOME/.config/studio-session-backup/config
					exec rb-scripts-box.sh -s
			elif [[ $MAIN_BOX == $OPTION_7 ]]; then
				SEL_BOX=$(zenity --file-selection --title="Choose DAW configs directory" --directory --multiple | sed 's|/|\\/|g')
					sed -i "/daw_config_dir/ s/$(grep -w daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | sed 's|/|\\/|g')/$SEL_BOX/" $HOME/.config/studio-session-backup/config
					exec rb-scripts-box.sh -s
			elif [[ $MAIN_BOX == $OPTION_8 ]]; then
				SEL_BOX=$(zenity --file-selection --title="Choose backup directory for DAW" --directory --multiple | sed 's|/|\\/|g')
					sed -i "/backup_daw_config_dir/ s/$(grep -w backup_daw_config_dir $HOME/.config/studio-session-backup/config | cut -d "=" -f 2 | sed 's|/|\\/|g')/$SEL_BOX/" $HOME/.config/studio-session-backup/config
					exec rb-scripts-box.sh -s
			fi
}

power_manager_tray_icon() {
if [[ $(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/show-tray-icon -v) == "true" ]]; then
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/show-tray-icon -s false
elif [[ $(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/show-tray-icon -v) == "false" ]]; then
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/show-tray-icon -s true
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

power_manager_presentation_mode() {
if [[ $(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/presentation-mode -v) == "true" ]]; then
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/presentation-mode -s false
	notify-send --icon=desktop -t 3600 "Presentation Mode Off"
elif [[ $(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/presentation-mode -v) == "false" ]]; then
	xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/presentation-mode -s true
	notify-send --icon=desktop -t 3600 "Presentation Mode On"
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

toggle_touchpad () {
if [[ $(xfconf-query -c pointers -lv | grep -w Device_Enabled | grep -v TrackPoint | awk '{print $2}') -eq 1 ]]; then
	xfconf-query -c pointers -p /Synaptics_tm2964-001/Properties/Device_Enabled -s 0
	notify-send --icon=touchpad -t 3600 "Touchpad disabled."
elif [[ $(xfconf-query -c pointers -lv | grep -w Device_Enabled | grep -v TrackPoint | awk '{print $2}') -eq 0 ]]; then
	xfconf-query -c pointers -p /Synaptics_tm2964-001/Properties/Device_Enabled -s 1
	notify-send --icon=touchpad -t 3600 "Touchpad enabled."
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

toggle_compositor() {
if [[ $(xfconf-query -c xfwm4 -p /general/use_compositing -v) == "true" ]]; then
	xfconf-query -c xfwm4 -p /general/use_compositing -s false
elif [[ $(xfconf-query -c xfwm4 -p /general/use_compositing -v) == "false" ]]; then
	xfconf-query -c xfwm4 -p /general/use_compositing -s true
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

toggle_networking() {
if [[ $(nmcli n) == "enabled" ]]; then
	nmcli n off
	notify-send --icon=dialog-information -t 3600 "Networking Disabled."
elif [[ $(nmcli n) == "disabled" ]]; then
	nmcli n on
	notify-send --icon=dialog-information -t 3600 "Networking Enabled."
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

toggle_wifi() {
if [[ $(nmcli r wifi) == "enabled" ]]; then
	nmcli r wifi off
	notify-send --icon=dialog-information -t 3600 "WiFi Disabled."
elif [[ $(nmcli r wifi) == "disabled" ]]; then
	nmcli r wifi on
	notify-send --icon=dialog-information -t 3600 "WiFi Enabled."
else
	notify-send --icon=dialog-information -t 4600 "Error: wrong syntax."
fi
}

case "$1" in
	--check-updates | -c ) check_updates;;
	--places | -p ) places;;
	--backup-studio | -s ) backup_studio_session;;
	--toggle-pm-tray-icon | -tpmti ) power_manager_tray_icon;;
	--toggle-presentation-mode | -tpm ) power_manager_presentation_mode;;
	--toggle-touchpad | -ttp ) toggle_touchpad;;
	--toggle-compositor | -tc ) toggle_compositor;;
	--toggle-networking | -tn ) toggle_networking;;
	--toggle-wifi | -twifi ) toggle_wifi;;
	--version | -v ) echo -e "\n    Version 1.0\n";;
	*)
echo -e "
	Usage:\n
	rb-scripts-box.sh [ OPTION ]\n
	Options:\n
	-p, --places            Open custom jgmenu with Places.
	-W, --wise-update    Backup and Updadate.
	-s, --backup-studio  Backup your records (rsync).
	-tpmti, --toggle-pm-tray-icon
	-tpm, --toggle-presentation-mode
	-ttp, --toggle-touchpad
	-tc, --toggle-compositor
	-tn, --toggle-networking
	-twifi, --toggle-wifi
	-v, --version
" >&2
exit 1;;
esac
exit 0
