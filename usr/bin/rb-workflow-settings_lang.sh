#!/usr/bin/env bash

SYS_LANG=$(echo $LANG | cut -d '.' -f 1)
if [[ -z $(find /usr/share/workflow/translations/settings/ -name $SYS_LANG*) ]]; then
	source /usr/share/workflow/translations/settings/en_US.trans
else
	for TRANS in /usr/share/workflow/translations/settings/$SYS_LANG*; do
		source $TRANS
	done
fi
