#!/usr/bin/env bash

side_menu() {
PANEL_HEIGHT=$(expr 6 + $(xprop -root _NET_WORKAREA | awk -F ',' '{print $2}' | sed 's/^.//'))
MENU_HEIGHT=$(expr $(xprop -root _NET_WORKAREA | awk -F ',' '{print $4}' | sed 's/^.//') - 12)
sed -i "s/$(grep menu_height_min $HOME/.config/side-menu/sidemenu-lightrc)/menu_height_min = $MENU_HEIGHT/" $HOME/.config/side-menu/sidemenu-lightrc
sed -i "s/$(grep menu_height_max $HOME/.config/side-menu/sidemenu-lightrc)/menu_height_max = $MENU_HEIGHT/" $HOME/.config/side-menu/sidemenu-lightrc
sed -i "s/$(grep menu_height_min $HOME/.config/side-menu/sidemenu-darkrc)/menu_height_min = $MENU_HEIGHT/" $HOME/.config/side-menu/sidemenu-darkrc
sed -i "s/$(grep menu_height_max $HOME/.config/side-menu/sidemenu-darkrc)/menu_height_max = $MENU_HEIGHT/" $HOME/.config/side-menu/sidemenu-darkrc
sed -i "/menu_margin_y/ s/$(grep menu_margin_y $HOME/.config/side-menu/sidemenu-lightrc | awk '{print $3}')/$PANEL_HEIGHT/" $HOME/.config/side-menu/sidemenu-lightrc
sed -i "/menu_margin_y/ s/$(grep menu_margin_y $HOME/.config/side-menu/sidemenu-darkrc | awk '{print $3}')/$PANEL_HEIGHT/" $HOME/.config/side-menu/sidemenu-darkrc
jgmenu --config-file="$HOME/.config/side-menu/$(grep 'side_menurc' $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file="$HOME/.config/side-menu/sidemenu-config.csv"
}

### Network Menu items
network_menu() {
ETHERNET=$(nmcli --terse --fields STATE,TYPE dev status | grep ethernet | sed 's/:.*//')
WIFI_TEST=$(nmcli radio wifi)
VPN_TEST=$(nmcli --terse --fields TYPE con show | grep vpn | head -1 | sed 's/:.*//')
MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT
cat <<'EOF' > ${MENU_ITEMS}
^sep()
^sep(Wired)
^sep()
EOF
	nmcli --terse --fields NAME,TYPE con show -a | grep -v 'wireless\|vpn\|tun' | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^/    /' | sed "s/$/' \&\& notify-send --icon=network-modem -t 3600 'Wire connection Deactivated.'/" >> ${MENU_ITEMS}
	nmcli --terse --fields NAME,TYPE,DEVICE con show | grep ethernet | grep -v 'enp' | sed 's/:.*//' | sed "s/$/,nmcli con up '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con up '$//" | sed 's/^/   /' | sed "s/$/' \&\& notify-send --icon=network-modem -t 3600 'Wire connection Activated.'/" >> ${MENU_ITEMS}

if [[ "$WIFI_TEST" == "enabled" ]]; then
cat <<'EOF' >> ${MENU_ITEMS}
^sep()
^sep(Wi-Fi)
^sep()
EOF
	nmcli --terse --fields NAME,TYPE con show -a | grep wireless | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^/    /' | sed "s/$/' \&\& notify-send --icon=network-wireless -t 3600 'WiFi connection Deactivated.'/" >> ${MENU_ITEMS}
	nmcli --terse --fields SSID,IN-USE dev wifi list | grep -v '*' | sed 's/:.*//' | sed "s/$/,nmcli dev wifi con '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli dev wifi con '$//" | sed 's/^/   /' | sed "s/$/' \&\& notify-send --icon=network-wireless -t 3600 'WiFi connection Activated.'/" >> ${MENU_ITEMS}
else
cat <<'EOF' >> ${MENU_ITEMS}
^sep()
EOF
fi

if [[ "$VPN_TEST" == "vpn" ]]; then
cat <<'EOF' >> ${MENU_ITEMS}
^sep()
^sep(VPN)
^sep()
EOF
	nmcli --terse --fields NAME,TYPE con show -a | grep vpn | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^/    /' | sed "s/$/' \&\& notify-send --icon=network-vpn -t 3600 'VPN connection Deactivated.'/" >> ${MENU_ITEMS}
	nmcli --terse --fields NAME,TYPE,DEVICE con show | grep vpn | grep -v 'wlp\|enp' | sed 's/:.*//' | sed "s/$/,nmcli con up '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con up '$//" | sed 's/^/   /' | sed "s/$/' \&\& notify-send --icon=network-vpn -t 3600 'VPN connection Activated.'/" >> ${MENU_ITEMS}
else
cat <<'EOF' >> ${MENU_ITEMS}
^sep()
EOF
fi

cat <<'EOF' >> ${MENU_ITEMS}
^sep(Toggle)
    Networking,rb-scripts-box.sh -tn
    WiFi,rb-scripts-box.sh -twifi
    Network Applet,rb-side-menu.sh -a
^sep()
    Network Connection Editor,nm-connection-editor
^sep()
    Back,rb-side-menu.sh -s
^sep()
EOF
jgmenu --config-file="$HOME/.config/side-menu/$(grep 'side_menurc' $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file=${MENU_ITEMS}
}

netwoek_applet() {
	if [[ -z $(pgrep --uid $USER -x nm-applet) ]]; then
		exec nm-applet
	else
		kill 15 $(pgrep --uid $USER -x nm-applet)
	fi
}

case "$1" in
	--side-menu | -s ) side_menu;;
	--network-menu | -n ) network_menu;;
	--network-applet | -a ) netwoek_applet;;
	--version | -v ) echo -e "\n    Version 1.0\n";;
	*)
echo -e "
	Usage:\n
	rb-side-menu.sh [ OPTION ]\n
	Options:\n
	--side-menu, -s         execute sidemenu menu
	--network-menu, -n      execute network menu\n
	--network-applet, -a        toggle nm-applet
	--version, -v           show script version
" >&2
exit 1;;
esac
exit 0
