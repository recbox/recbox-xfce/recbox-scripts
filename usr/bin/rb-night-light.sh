#!/usr/bin/env bash

STATUS=$(grep redshift_status $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
SETTINGS_PATH="$HOME/.config/night-light-settings/config"
SIDEMENU_PATH="$HOME/.config/side-menu/sidemenu-config.csv"
FONT_ICON_ON=$(grep font_icon_on $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
FONT_ICON_OFF=$(grep font_icon_off $HOME/.config/night-light-settings/config | cut -d '=' -f 2)

run() {
COLOR_TEMP=$(grep color_temp $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
BRIGHTNESS=$(grep brightness $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
GAMMA=$(grep gamma $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
METHOD=$(grep method $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
	if [[ $STATUS == "On" ]]; then
		redshift -x -m $METHOD
		sed -i "/redshift_status/ s/$STATUS/Off/" "$SETTINGS_PATH"
		sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_PATH"
	elif [[ $STATUS == "Off" ]]; then
		redshift -O $COLOR_TEMP -g $GAMMA -b $BRIGHTNESS -m $METHOD -P
		sed -i "/redshift_status/ s/$STATUS/On/" "$SETTINGS_PATH"
		sed -i "s/$FONT_ICON_OFF/$FONT_ICON_ON/" "$SIDEMENU_PATH"
	else
		echo "error"
	fi
}

restart() {
COLOR_TEMP=$(grep color_temp $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
BRIGHTNESS=$(grep brightness $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
GAMMA=$(grep gamma $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
METHOD=$(grep method $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
	redshift -O $COLOR_TEMP -g $GAMMA -b $BRIGHTNESS -m $METHOD -P
}

reset_status() {
	if [[ $STATUS == "On" ]]; then
		sed -i "/redshift_status/ s/$STATUS/Off/" "$SETTINGS_PATH"
		sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_PATH"
	else
		echo "error"
	fi
}

settings() {
MAIN_MENU_TITLE="RecBox - Night Light"
TEMP_WIDGET_TITLE="Color Temperature"
BRGHT_WIDGET_TITLE="Display Brightness"
MAIN_MENU_TEXT="Night Light use Redshift to change color temperature, brightness\nand gamma in One-shot manual mode. See 'man redshift'.\n"
COLOR_TEMP_TEXT="Adjust color temperature value"
BRIGHTNESS_TEXT="Adjust brightness value"
GAMMA_TEXT="Adjust gamma value"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Back"
COLUMN_1="Option"
COLUMN_2="Description"
SEPARATOR=""
SPACE=""
OPTION_1="Color Temperature"
OPTION_2="Display Brightness"
OPTION_3="Gamma"
OPTION_1_DESCRIPTION="From 3000K to 6500k (default - 6500k)"
OPTION_2_DESCRIPTION="From 0.1 to 1.0 (default - 1.0)"
OPTION_3_DESCRIPTION="With RGB (default - 1:1:1)"

ICON_PATH=$(grep icon_path $HOME/.config/night-light-settings/config | cut -d '=' -f 2)
CONFIG_PATH="$HOME/.config/night-light-settings/config"
TEMP_WIDGET_VALUE=$(grep color_temp "$CONFIG_PATH" | cut -d '=' -f 2)
BRGHT_WIDGET_VALUE=$(grep brightness "$CONFIG_PATH" | cut -d '=' -f 2 | cut -d '.' -f 2)
GAMMA_WIDGET_VALUE=$(grep gamma "$CONFIG_PATH" | cut -d '=' -f 2 | cut -d '.' -f 2)

ZEN_MENU="zenity --list --width=512 --height=222"
TEMP_WIDGET="zenity --scale --min-value=3000 --max-value=6500 --step=1"
BRGHT_WIDGET="zenity --scale --min-value=1 --max-value=10 --step=10"

	BOX=$($ZEN_MENU --title="$MAIN_MENU_TITLE" --text="$MAIN_MENU_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$SEPARATOR" --column="$COLUMN_2" \
	"$OPTION_1" "$SPACE" "$OPTION_1_DESCRIPTION" \
	"$OPTION_2" "$SPACE" "$OPTION_2_DESCRIPTION" \
	"$OPTION_3" "$SPACE" "$OPTION_3_DESCRIPTION" \
	--window-icon="$ICON_PATH")

	case $? in
		0 )
			if [[ $BOX == $OPTION_1 ]]; then
				TEMP_BOX=$($TEMP_WIDGET --value="$TEMP_WIDGET_VALUE" --window-icon="$ICON_PATH" --title="$TEMP_WIDGET_TITLE" \
				--text="$COLOR_TEMP_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
					case $? in
						0 )
							if [[ -z $TEMP_BOX ]]; then
								exit
							elif [[ $TEMP_BOX ]]; then
								sed -i "/color_temp/ s/$TEMP_WIDGET_VALUE/$TEMP_BOX/" "$CONFIG_PATH"
									if [[ $STATUS == "On" ]]; then
										restart && settings
									elif [[ $STATUS == "Off" ]]; then
										settings
									fi
							fi
							;;
						1 )
							if [[ $TEMP_BOX == $EXTRA_BUTTON ]]; then
								settings
								else
									exit
							fi
							;;
						-1 ) echo "Color temperature widget error";;
					esac
			fi

			if [[ $BOX == $OPTION_2 ]]; then
				if [[ $BRGHT_WIDGET_VALUE -eq 0  ]]; then
					BRGHT_WIDGET_VALUE="10"
				fi
					BRGHT_BOX=$($BRGHT_WIDGET --value="$BRGHT_WIDGET_VALUE" --window-icon="$ICON_PATH" --title="$BRGHT_WIDGET_TITLE" \
					--text="$BRIGHTNESS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
						case $? in
							0 )
								if [[ -z $BRGHT_BOX ]]; then
									exit
								elif [[ $BRGHT_BOX -eq 10 ]]; then
									sed -i "s/$(grep brightness "$CONFIG_PATH")/brightness=1.0/" "$CONFIG_PATH"
										if [[ $STATUS == "On" ]]; then
											restart && settings
										elif [[ $STATUS == "Off" ]]; then
											settings
										fi
								elif [[ $BRGHT_BOX -lt 10 ]]; then
									sed -i "s/$(grep brightness "$CONFIG_PATH")/brightness=0.$BRGHT_BOX/" "$CONFIG_PATH"
										if [[ $STATUS == "On" ]]; then
											restart && settings
										elif [[ $STATUS == "Off" ]]; then
											settings
										fi
								fi
								;;
							1 )
								if [[ $BRGHT_BOX == $EXTRA_BUTTON ]]; then
									settings
									else
										exit
								fi
								;;
							-1 ) echo "Brightness widget error";;
						esac
			fi

			if [[ $BOX == $OPTION_3 ]]; then
				if [[ $GAMMA_WIDGET_VALUE -eq 0  ]]; then
					GAMMA_WIDGET_VALUE="10"
				fi
					GAMMA_BOX=$($BRGHT_WIDGET --value="$GAMMA_WIDGET_VALUE" --window-icon="$ICON_PATH" --title="$BRGHT_WIDGET_TITLE" \
					--text="$GAMMA_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
						case $? in
							0 )
								if [[ -z $GAMMA_BOX ]]; then
									exit
								elif [[ $GAMMA_BOX -eq 10 ]]; then
									sed -i "s/$(grep gamma "$CONFIG_PATH")/gamma=1.0/" "$CONFIG_PATH"
										if [[ $STATUS == "On" ]]; then
											restart && settings
										elif [[ $STATUS == "Off" ]]; then
											settings
										fi
								elif [[ $GAMMA_BOX -lt 10 ]]; then
									sed -i "s/$(grep gamma "$CONFIG_PATH")/gamma=0.$GAMMA_BOX/" "$CONFIG_PATH"
										if [[ $STATUS == "On" ]]; then
											restart && settings
										elif [[ $STATUS == "Off" ]]; then
											settings
										fi
								fi
								;;
							1 )
								if [[ $GAMMA_BOX == $EXTRA_BUTTON ]]; then
									settings
									else
										exit
								fi
								;;
							-1 ) echo "Gamma widget error";;
						esac
			fi
			;;
		1 )	exit;;
		-1 ) echo "error";;
		* )
	esac
}

case "$1" in
	--run | -r ) run;;
	--reset-status | -R ) reset_status;;
	--settings | -s ) settings;;
	--version | -v ) echo -e "\n    Version 1.0\n";;
	*)
echo -e "
	Usage:\n
	rb-night-light.sh [ OPTION ]\n
	Options:\n
	 -r, --run               Run Redshift in one-shot mode.
	 -R, --reset-status      Reset Night Light status on Shutdown, Restart,
				 Switch User, Log Out.
	 -s, --settings          Open Night Light Settings menu.
	 -v, --version           Show script version.
" >&2
exit 1;;
esac
exit 0
