#!/usr/bin/env bash

updates_menu()  {
echo -e "
:: What you want to do?\n
   Options:
   Check Updates           [ 0 ]  [ c/C ]  [ check ]
   Update Software         [ 1 ]  [ u/U ]  [ update ]
   Download Packages       [ 2 ]  [ d/D ]  [ download ]
   Backup and Update       [ 3 ]  [ b/B ]  [ backup ]
   Update Mirrors List     [ 4 ]  [ m/M ]  [ mirrors ]
   Refresh Database        [ 5 ]  [ f/F ]  [ force-refresh ]
   Restart                 [ 6 ]  [ r/R ]  [ restart ]
   Exit                    [  Enter or other characters ]"
   read -r -p "   Answer: " input
    case $input in
      0 | [cC] | check ) check_updates;;
			1 | [uU] | update ) perform_update;;
      2 | [dD] | download ) download_packages;;
			3 | [bB] | backup ) backup_update;;
			4 | [mM] | mirrors ) refresh_mirrors;;
      5 | [fF] | force-refresh ) refresh_database;;
			6 | [rR] | restart ) xfce4-session-logout --reboot --fast;;
			* ) exit 0;;
		esac
}

check_updates() {
MIRRORS=$(grep -w mirrors_list $HOME/.config/mirrors-list-cli/config | cut -d '=' -f2)
	echo ":: Waiting for Mirrors Status:"; pacman-mirrors --status | awk '{$8=""; print $0}'; echo -e "\n:: Checking for Updates:"; \
	pamac checkupdates; echo -e "\n:: Disk Usage Status:"; df -h | grep -v tmpfs | grep -v run; updates_menu
}

perform_update() {
echo -e "\n:: Checking for Updates"; pamac update --no-confirm; updates_menu
}

download_packages() {
echo -e "\n:: Downloading packages"; pamac update --no-confirm --download-only; updates_menu
}

backup_update() {
echo -e "\n:: Starting Timeshift to create system backup"
pkexec timeshift --create
echo -e "\n:: Performing system update"
pamac update --no-confirm; updates_menu
}

refresh_database() {
echo -e "\n:: Refreshing database and update"
pamac update --no-confirm --force-refresh; updates_menu
}

refresh_mirrors() {
if [[ -z $MIRRORS ]]; then
	clear
	echo -e "\n:: Configuration file not found\n:: Creating config file"
	mkdir -p $HOME/.config/mirrors-list-cli; echo "mirrors_list=pacman-mirrors --fasttrack 5" > $HOME/.config/mirrors-list-cli/config
	updates_menu
else
	echo -e "\n:: Updating Mirrors list"; pkexec $MIRRORS; updates_menu
fi
}

case "$1" in
  -c | --check ) check_updates;;
  -u | --update ) perform_update;;
  -d | --download ) download_packages;;
  -b | --backup ) backup_update;;
  -m | --mirrors ) refresh_mirrors;;
  -r | --restart ) xfce4-session-logout --reboot --fast;;
  -f | --force-refresh ) refresh_database;;
  -v | --version ) echo -e "\n    Version 1.0\n";;
  * )

echo -e "
    Usage:\n
    rb-pamac.sh [ OPTION ]\n
    Options:\n
    -c, --check            check for updates
    -u, --update           update system
    -d, --download         download packages only
    -b, --backup           backupa system and update
    -m, --mirrors          refresh mirrors lisk and update
    -f, --force-refresh    refresh database and update
    -r, --restart ) resrart system
    -v, --version
" >&2
exit 1;;
esac
exit 0
