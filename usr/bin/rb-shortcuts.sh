#!/usr/bin/env bash

# Translation
source /usr/bin/rb-shortcuts_lang.sh
# Create shortcuts list
COLUMN_1=$(xfconf-query -c xfce4-keyboard-shortcuts -l -v | egrep -v 'Up|Down|Right|Left|Escape|override|providers|true' \
| awk -F ' ' '{print $1}' | awk -F '/' '{print $4}' | sed -e 's/>/ + /g' -e 's/<//g' -e 's/KP_//g')
# Create command list
COLUMN_2=$(xfconf-query -c xfce4-keyboard-shortcuts -l -v | egrep -v 'Up|Down|Right|Left|Escape|override|providers|true' \
| awk -F ' ' '{$1=""; print $0}' | sed -e 's/_key/ /g' -e 's/_/ /g' -e 's/^ //')
SPACE_1="        "
SPACE_2="    "

	BOX=$(zenity --list --column="$SPACE_1" --column="$COLMN1_TEXT" --column="$SPACE_2" --column="$COLMN2_TEXT" \
	--column="$SPACE_1" "$SPACE_1" "$COLUMN_1" "$SPACE_2" "$COLUMN_2" "$SPACE_1" --cancel-label="$CANCEL_BUTTON" \
	--extra-button="$EXTRA_BUTTON" --ok-label="$OK_BUTTON" --title="$TITLE" --text="$TEXT" --width=551 --height=400 \
	--window-icon="/usr/share/icons/recbox/recbox-xfce-shortcuts.svg")
		case $? in
			0 ) exec xfce4-keyboard-settings;;
			1 )
				if [[ $BOX == $EXTRA_BUTTON ]]; then
					exec xfwm4-settings
				else
					exit
				fi
				;;
			* )
		esac
