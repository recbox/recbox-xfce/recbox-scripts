#!/usr/bin/env bash

stable-updates() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

wget --quiet -O - https://forum.manjaro.org/c/announcements/stable-updates/12 | grep "itemprop='name' content=" | head -3 | sed "s/'>//g" | sed 's/,//g' | sed 's/.*] //' > ${MENU_ITEMS}
wget --quiet -O - https://forum.manjaro.org/c/announcements/stable-updates/12 | sed -n -e 's|.*https://forum.manjaro.org/t/\(.*\)class=.*|\1|p' | head -3 | sed 's/..$//' | sed 's|^|,exo-open https://forum.manjaro.org/t/|' >> ${MENU_ITEMS}

sed -i '2{N;h;d};4G' ${MENU_ITEMS}
sed -i -n '4{h;n;G};p' ${MENU_ITEMS}
sed -i 'N;s/\n/ /' ${MENU_ITEMS}
sed -i 's/^//' ${MENU_ITEMS}

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
Back,rb-side-menu.sh -s
EOF

jgmenu --config-file="$HOME/.config/side-menu/$(grep 'side_menurc' $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file=${MENU_ITEMS}

}

testing-updates() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

wget --quiet -O - https://forum.manjaro.org/c/announcements/testing-updates/13 | grep "itemprop='name' content=" | head -3 | sed "s/'>//g" | sed 's/,//g' | sed 's/.*] //' > ${MENU_ITEMS}
wget --quiet -O - https://forum.manjaro.org/c/announcements/testing-updates/13 | sed -n -e 's|.*https://forum.manjaro.org/t/\(.*\)class=.*|\1|p' | head -3 | sed 's/..$//' | sed 's|^|,exo-open https://forum.manjaro.org/t/|' >> ${MENU_ITEMS}

sed -i '2{N;h;d};4G' ${MENU_ITEMS}
sed -i -n '4{h;n;G};p' ${MENU_ITEMS}
sed -i 'N;s/\n/ /' ${MENU_ITEMS}
sed -i 's/^//' ${MENU_ITEMS}

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
Back,rb-side-menu.sh -s
EOF

jgmenu --config-file="$HOME/.config/side-menu/$(grep 'side_menurc' $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file=${MENU_ITEMS}

}

unstable-updates() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

wget --quiet -O - https://forum.manjaro.org/c/announcements/unstable-updates/15 | grep "itemprop='name' content=" | head -3 | sed "s/'>//g" | sed 's/,//g' | sed 's/.*] //' > ${MENU_ITEMS}
wget --quiet -O - https://forum.manjaro.org/c/announcements/unstable-updates/15 | sed -n -e 's|.*https://forum.manjaro.org/t/\(.*\)class=.*|\1|p' | head -3 | sed 's/..$//' | sed 's|^|,exo-open https://forum.manjaro.org/t/|' >> ${MENU_ITEMS}

sed -i '2{N;h;d};4G' ${MENU_ITEMS}
sed -i -n '4{h;n;G};p' ${MENU_ITEMS}
sed -i 'N;s/\n/ /' ${MENU_ITEMS}
sed -i 's/^//' ${MENU_ITEMS}

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
Back,rb-side-menu.sh -s
EOF

jgmenu --config-file="$HOME/.config/side-menu/$(grep 'side_menurc' $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)" --csv-file=${MENU_ITEMS}

}

case "$1" in

	--stable-updates | -s ) stable-updates;;
	--testing-updates | -t ) testing-updates;;
	--unstable-updates| -u ) unstable-updates;;
	--version | -v ) echo -e "\n    Version 1.0\n";;
	*)

echo -e "
	Usage:\n
	rb-manjaro-announcements.sh [ OPTION ]\n
	Options:\n
	 -s, --stable-updates      Show Announcements from Stable branch.
	 -t, --testing-updates     Show Announcements from Testing branch.
	 -u, --unstable-updates    Show Announcements from Unstable branch.
	 -v, --version
" >&2
exit 1;;
esac
exit 0
