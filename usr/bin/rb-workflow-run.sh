#!/usr/bin/env bash

# Translation
source /usr/bin/rb-workflow-run_lang.sh

CONF_PATH="$HOME/.config/workflow-settings/conf"

daily_mode() {
	if [[ -z $(find $HOME/.config -type d -name workflow-settings) ]]; then
		zenity --error --title="$ERROR_WIDGET_TITLE" --text="$ERROR_WIDGET_TEXT" --no-wrap
	else
		echo -e ":: $SYSTEM_SETUP_START\n:: $DAILY_MODE\n==> $SESSION_AND_STARTUP"
		rb-workflow-settings.sh --reconf-daily-desktop 1> /dev/null
		rb-workflow-desktop.sh --daily
		mkdir -p /tmp/workflow; echo "USER=$(echo $USER)" | tee /tmp/workflow/user > /dev/null
		echo "==> $SESSION_AND_STARTUP_COMPLETE"
		echo "==> $SYSTEMD_SERVICES"
		pkexec rb-workflow-system.sh --daily
		if [[ -z $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') ]]; then
			echo "mode=daily" > $CONF_PATH
		else
			if [[ $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') == "studio" ]]; then
				sed -i '/mode/ s/studio/daily/' $CONF_PATH
			fi
		fi
		echo -e "\n:: $DAILY_MODE_COMPLETE\n==> $RESTART_REQUIRED\n"
		read -p ":: $ENTER_TO_EXIT "
	fi
}

studio_mode() {
	if [[ -z $(find $HOME/.config -type d -name workflow-settings) ]]; then
		zenity --error --title="$ERROR_WIDGET_TITLE" --text="$ERROR_WIDGET_TEXT" --no-wrap
	else
		echo -e ":: $SYSTEM_SETUP_START\n:: $STUDIO_MODE\n==> $SESSION_AND_STARTUP"
		rb-workflow-settings.sh --reconf-studio-desktop 1> /dev/null
		rb-workflow-desktop.sh --studio
		mkdir -p /tmp/workflow; echo "USER=$(echo $USER)" | tee /tmp/workflow/user > /dev/null
		echo "==> $SESSION_AND_STARTUP_COMPLETE"
		echo "==> $SYSTEMD_SERVICES"
		pkexec rb-workflow-system.sh --studio
		if [[ -z $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') ]]; then
			echo "mode=studio" > $CONF_PATH
		else
			if [[ $(grep -w 'mode' $CONF_PATH | awk -F '=' '{print $2}') == "daily" ]]; then
				sed -i '/mode/ s/daily/studio/' $CONF_PATH
			fi
		fi
		echo -e "\n:: $STUDIO_MODE_COMPLETE\n==> $RESTART_REQUIRED\n"
		read -p ":: $ENTER_TO_EXIT "
	fi
}

case "$1" in
	--daily-mode | -d ) daily_mode;;
	--studio-mode | -s ) studio_mode;;
	*)
echo -e "
	Usage:\n
	rb-workflow-run.sh [ OPTION ]\n
	Options:\n
	--daily-mode, -d
	--studio-mode, -s
" >&2
exit 1;;
esac
exit 0
