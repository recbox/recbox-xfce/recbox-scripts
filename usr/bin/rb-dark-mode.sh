#!/usr/bin/env bash

DARK_MODE_STATUS=$(grep -w dark_mode_status $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
CONFIG_PATH="$HOME/.config/darkmode-settings/config"
SIDEMENU_CONFIG_PATH="$HOME/.config/side-menu/sidemenu-config.csv"
SIDEMENU_LIGHT_PATH="$HOME/.config/side-menu/sidemenu-lightrc"
SIDEMENU_DARK_PATH="$HOME/.config/side-menu/sidemenu-darkrc"
ICON_PATH=$(grep -w window_icon $HOME/.config/darkmode-settings/config | awk -F '=' '{print $2}')
QT_CONFIG_PATH="$HOME/.config/qt5ct/qt5ct.conf"

SIDEMENU_LIGHT_BG=$(grep -w color_menu_bg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_NORM_BG=$(grep -w color_norm_bg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_NORM_FG=$(grep -w color_norm_fg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_SEL_BG=$(grep -w color_sel_bg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_SEL_FG=$(grep -w color_sel_fg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_SEP_FG=$(grep -w color_sep_fg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_SCROLL_IND=$(grep -w color_scroll_ind "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_TITLE_FG=$(grep -w color_title_fg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_TITLE_BG=$(grep -w color_title_bg "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)
SIDEMENU_LIGHT_TITLE_BORDER=$(grep -w color_title_border "$SIDEMENU_LIGHT_PATH" | cut -d ' ' -f 3)

SIDEMENU_DARK_BG=$(grep -w color_menu_bg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_NORM_BG=$(grep -w color_norm_bg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_NORM_FG=$(grep -w color_norm_fg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_SEL_BG=$(grep -w color_sel_bg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_SEL_FG=$(grep -w color_sel_fg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_SEP_FG=$(grep -w color_sep_fg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_SCROLL_IND=$(grep -w color_scroll_ind "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_TITLE_FG=$(grep -w color_title_fg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_TITLE_BG=$(grep -w color_title_bg "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)
SIDEMENU_DARK_TITLE_BORDER=$(grep -w color_title_border "$SIDEMENU_DARK_PATH" | cut -d ' ' -f 3)

sidemenu_light_config() {
GTK_THEME_LIGHT=$(grep -w gtk_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
if [[ $GTK_THEME_LIGHT == "Adwaita" ]]; then
	sed -i "/color_menu_bg/ s/$SIDEMENU_LIGHT_BG/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_norm_bg/ s/$SIDEMENU_LIGHT_NORM_BG/#ffffff/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_norm_fg/ s/$SIDEMENU_LIGHT_NORM_FG/#363636/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_sel_bg/ s/$SIDEMENU_LIGHT_SEL_BG/#3584E4/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_sel_fg/ s/$SIDEMENU_LIGHT_SEL_FG/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_sep_fg/ s/$SIDEMENU_LIGHT_SEP_FG/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_scroll_ind/ s/$SIDEMENU_LIGHT_SCROLL_IND/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_title_fg/ s/$SIDEMENU_LIGHT_TITLE_FG/#363636/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_title_bg/ s/$SIDEMENU_LIGHT_TITLE_BG/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
	sed -i "/color_title_border/ s/$SIDEMENU_LIGHT_TITLE_BORDER/#f7f7f7/" "$SIDEMENU_LIGHT_PATH"
else
	if [[ -z $(find /usr/share/themes/* -name $GTK_THEME_LIGHT) ]]; then
		BACKGROUND_COLOR=$(grep -w '@define-color theme_bg_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		NORMAL_BACKGROUND_COLOR=$(grep -w '@define-color theme_base_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_COLOR=$(grep -w '@define-color theme_selected_fg_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_TITLE=$(grep -w '@define-color theme_text_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_BACKGROUND=$(grep -w '@define-color theme_selected_bg_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
			if [[ -z $(grep -w '@define-color theme_bg_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_menu_bg/ s/$SIDEMENU_LIGHT_BG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_sel_fg/ s/$SIDEMENU_LIGHT_SEL_FG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_sep_fg/ s/$SIDEMENU_LIGHT_SEP_FG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_scroll_ind/ s/$SIDEMENU_LIGHT_SCROLL_IND/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_border/ s/$SIDEMENU_LIGHT_TITLE_BORDER/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_bg/ s/$SIDEMENU_LIGHT_TITLE_BG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_base_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_norm_bg/ s/$SIDEMENU_LIGHT_NORM_BG/$TEXT_COLOR/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_selected_bg_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_sel_bg/ s/$SIDEMENU_LIGHT_SEL_BG/$TEXT_BACKGROUND/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text background needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_text_color' $HOME/.themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_norm_fg/ s/$SIDEMENU_LIGHT_NORM_FG/$TEXT_TITLE/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_fg/ s/$SIDEMENU_LIGHT_TITLE_FG/$TEXT_TITLE/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for title text needs to be set manually.")"
			fi
	else
		BACKGROUND_COLOR=$(grep -w '@define-color theme_bg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		NORMAL_BACKGROUND_COLOR=$(grep -w '@define-color theme_base_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_COLOR=$(grep -w '@define-color theme_selected_fg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_TITLE=$(grep -w '@define-color theme_text_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
		TEXT_BACKGROUND=$(grep -w '@define-color theme_selected_bg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | awk '{print $3}' | sed 's/;$//')
			if [[ -z $(grep -w '@define-color theme_bg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_menu_bg/ s/$SIDEMENU_LIGHT_BG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_sel_fg/ s/$SIDEMENU_LIGHT_SEL_FG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_sep_fg/ s/$SIDEMENU_LIGHT_SEP_FG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_scroll_ind/ s/$SIDEMENU_LIGHT_SCROLL_IND/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_bg/ s/$SIDEMENU_LIGHT_TITLE_BG/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_border/ s/$SIDEMENU_LIGHT_TITLE_BORDER/$BACKGROUND_COLOR/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_selected_fg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_norm_bg/ s/$SIDEMENU_LIGHT_NORM_BG/$TEXT_COLOR/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_selected_bg_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_sel_bg/ s/$SIDEMENU_LIGHT_SEL_BG/$TEXT_BACKGROUND/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text background needs to be set manually.")"
			fi
			if [[ -z $(grep -w '@define-color theme_text_color' /usr/share/themes/$GTK_THEME_LIGHT/gtk-3.0/gtk.css | grep -v "#") ]]; then
				sed -i "/color_norm_fg/ s/$SIDEMENU_LIGHT_NORM_FG/$TEXT_TITLE/" "$SIDEMENU_LIGHT_PATH"
				sed -i "/color_title_fg/ s/$SIDEMENU_LIGHT_TITLE_FG/$TEXT_TITLE/" "$SIDEMENU_LIGHT_PATH"
			else
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for title text needs to be set manually.")"
			fi
	fi
fi
}

sidemenu_dark_config() {
GTK_THEME_DARK=$(grep -w gtk_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
if [[ $GTK_THEME_DARK == "Adwaita-dark" ]]; then
	sed -i "/color_menu_bg/ s/$SIDEMENU_DARK_BG/#232729/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_norm_bg/ s/$SIDEMENU_DARK_NORM_BG/#33393B/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_norm_fg/ s/$SIDEMENU_DARK_NORM_FG/#ffffff/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_sel_bg/ s/$SIDEMENU_DARK_SEL_BG/#3584E4/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_sel_fg/ s/$SIDEMENU_DARK_SEL_FG/#ffffff/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_sep_fg/ s/$SIDEMENU_DARK_SEP_FG/#232729/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_scroll_ind/ s/$SIDEMENU_DARK_SCROLL_IND/#232729/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_title_fg/ s/$SIDEMENU_DARK_TITLE_FG/#b5abab/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_title_bg/ s/$SIDEMENU_DARK_TITLE_BG/#232729/" "$SIDEMENU_DARK_PATH"
	sed -i "/color_title_border/ s/$SIDEMENU_DARK_TITLE_BORDER/#232729/" "$SIDEMENU_DARK_PATH"
else
	if [[ -z $(find /usr/share/themes/* -name $GTK_THEME_DARK) ]]; then
		BACKGROUND_COLOR=$(grep -w '@define-color theme_bg_color' $HOME/.themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		NORMAL_BACKGROUND_COLOR=$(grep -w '@define-color theme_base_color' $HOME/.themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_COLOR=$(grep -w '@define-color theme_selected_fg_color' $HOME/.themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_TITLE=$(grep -w '@define-color theme_text_color' $HOME/.themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_BACKGROUND=$(grep -w '@define-color theme_selected_bg_color' $HOME/.themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
			if [[ -z $BACKGROUND_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			else
				sed -i "/color_menu_bg/ s/$SIDEMENU_DARK_BG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_sep_fg/ s/$SIDEMENU_DARK_SEP_FG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_scroll_ind/ s/$SIDEMENU_DARK_SCROLL_IND/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_title_bg/ s/$SIDEMENU_DARK_TITLE_BG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_title_border/ s/$SIDEMENU_DARK_TITLE_BORDER/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $NORMAL_BACKGROUND_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			else
				sed -i "/color_norm_bg/ s/$SIDEMENU_DARK_NORM_BG/$NORMAL_BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $TEXT_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text needs to be set manually.")"
			else
				sed -i "/color_norm_fg/ s/$SIDEMENU_DARK_NORM_FG/$TEXT_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_sel_fg/ s/$SIDEMENU_DARK_SEL_FG/$TEXT_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $TEXT_BACKGROUND ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text background needs to be set manually.")"
			else
				sed -i "/color_sel_bg/ s/$SIDEMENU_DARK_SEL_BG/$TEXT_BACKGROUND/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z TEXT_TITLE ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for title text needs to be set manually.")"
			else
				sed -i "/color_title_fg/ s/$SIDEMENU_DARK_TITLE_FG/$TEXT_TITLE/" "$SIDEMENU_DARK_PATH"
			fi
	else
		BACKGROUND_COLOR=$(grep -w '@define-color theme_bg_color' /usr/share/themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		NORMAL_BACKGROUND_COLOR=$(grep -w '@define-color theme_base_color' /usr/share/themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_COLOR=$(grep -w '@define-color theme_selected_fg_color' /usr/share/themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_TITLE=$(grep -w '@define-color theme_text_color' /usr/share/themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
		TEXT_BACKGROUND=$(grep -w '@define-color theme_selected_bg_color' /usr/share/themes/$GTK_THEME_DARK/gtk-3.0/gtk.css | awk '{print substr($3, 1, length($3)-1)}' | awk '{if (substr($1,1,1) ~ /^[#]/ ) print $0 }')
			if [[ -z $BACKGROUND_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			else
				sed -i "/color_menu_bg/ s/$SIDEMENU_DARK_BG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_sep_fg/ s/$SIDEMENU_DARK_SEP_FG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_scroll_ind/ s/$SIDEMENU_DARK_SCROLL_IND/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_title_bg/ s/$SIDEMENU_DARK_TITLE_BG/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_title_border/ s/$SIDEMENU_DARK_TITLE_BORDER/$BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $NORMAL_BACKGROUND_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "Colors for background needs to be set manually.")"
			else
				sed -i "/color_norm_bg/ s/$SIDEMENU_DARK_NORM_BG/$NORMAL_BACKGROUND_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $TEXT_BACKGROUND ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text background needs to be set manually.")"
			else
				sed -i "/color_sel_bg/ s/$SIDEMENU_DARK_SEL_BG/$TEXT_BACKGROUND/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $TEXT_COLOR ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for text needs to be set manually.")"
			else
				sed -i "/color_norm_fg/ s/$SIDEMENU_DARK_NORM_FG/$TEXT_COLOR/" "$SIDEMENU_DARK_PATH"
				sed -i "/color_sel_fg/ s/$SIDEMENU_DARK_SEL_FG/$TEXT_COLOR/" "$SIDEMENU_DARK_PATH"
			fi
			if [[ -z $TEXT_TITLE ]]; then
				notify-send --icon=dialog-information -t 4600 "Incompatible theme" "$(echo -e "\nColors for title text needs to be set manually.")"
			else
				sed -i "/color_title_fg/ s/$SIDEMENU_DARK_TITLE_FG/$TEXT_TITLE/" "$SIDEMENU_DARK_PATH"
			fi
	fi
fi
}

settings_sync() {
	if [[ $DARK_MODE_STATUS == "On" ]]; then
		sed -i "/gtk_theme_dark/ s/$(grep -w gtk_theme_dark $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="ThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
		sed -i "/kvantum_theme_dark/ s/$(grep -w kvantum_theme_dark $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w theme $HOME/.config/Kvantum/kvantum.kvconfig | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
		sed -i "/icons_theme_dark/ s/$(grep -w icons_theme_dark $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="IconThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
		sed -i "/qt_icon_theme_dark/ s/$(grep -w qt_icon_theme_dark $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w icon_theme $HOME/.config/qt5ct/qt5ct.conf | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
		sed -i "/cursor_theme_dark/ s/$(grep -w cursor_theme_dark $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="CursorThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
	elif [[ $DARK_MODE_STATUS == "Off" ]]; then
		sed -i "/gtk_theme_light/ s/$(grep -w gtk_theme_light $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="ThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
		sed -i "/kvantum_theme_light/ s/$(grep -w kvantum_theme_light $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w theme $HOME/.config/Kvantum/kvantum.kvconfig | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
		sed -i "/icons_theme_light/ s/$(grep -w icons_theme_light $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="IconThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
		sed -i "/qt_icon_theme_light/ s/$(grep -w qt_icon_theme_light $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w icon_theme $HOME/.config/qt5ct/qt5ct.conf | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
		sed -i "/cursor_theme_light/ s/$(grep -w cursor_theme_light $CONFIG_PATH | cut -d '=' -f 2)/$(grep -w 'name="CursorThemeName"' $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml | awk -F '"' '{print $6}')/" "$CONFIG_PATH"
	fi
}

settings() {
GTK_THEME_LIGHT=$(grep -w gtk_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
GTK_THEME_DARK=$(grep -w gtk_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
KVANTUM_THEME_LIGHT=$(grep -w kvantum_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
KVANTUM_THEME_DARK=$(grep -w kvantum_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
ICON_THEME_LIGHT=$(grep -w icons_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
ICON_THEME_DARK=$(grep -w icons_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_ICON_THEME_LIGHT=$(grep -w qt_icon_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_ICON_THEME_DARK=$(grep -w qt_icon_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_CONFIG_THEME=$(grep -w icon_theme $HOME/.config/qt5ct/qt5ct.conf | cut -d '=' -f 2)
CURSOR_THEME_LIGHT=$(grep -w cursor_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
CURSOR_THEME_DARK=$(grep -w cursor_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)

TITLE="Dark Mode Settings"
TEXT="<b>GTK themes:</b>\n<b>light: </b>$GTK_THEME_LIGHT    <b>dark: </b>$GTK_THEME_DARK \
\n\n<b>Kvantum themes: </b>\n<b>light: </b>$KVANTUM_THEME_LIGHT    <b>dark: </b>$KVANTUM_THEME_DARK \
\n\n<b>Icon themes:</b>\n<b>light: </b>$ICON_THEME_LIGHT    <b>dark: </b>$ICON_THEME_DARK \
\n\n<b>Qt Icon themes:</b>\n<b>light: </b>$QT_ICON_THEME_LIGHT    <b>dark: </b>$QT_ICON_THEME_DARK \
\n\n<b>Cursor themes:</b>\n<b>light: </b>$CURSOR_THEME_LIGHT    <b>dark: </b>$CURSOR_THEME_DARK\n"
PICK_COLUMN_TITLE="Pick"
THEME_COLUMN_TITLE="Theme"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Back"
MAIN_MENU="zenity --list --width=440 --height=485 --hide-header"

GTK_LIGHT_ITEM="      GTK light theme"
GTK_DARK_ITEM="        GTK dark theme"
KVANTUM_LIGHT_ITEM="      Kvantum light theme"
KVANTUM_DARK_ITEM="        Kvantum dark theme"
ICON_LIGHT_ITEM="      Icon light theme"
ICON_DARK_ITEM="        Icon dark theme"
QT_ICON_LIGHT_ITEM="      Qt Icon light theme"
QT_ICON_DARK_ITEM="        Qt Icon dark theme"
CURSOR_LIGHT_ITEM="      Cursor light theme"
CURSOR_DARK_ITEM="        Cursor dark theme"

	MAIN_BOX=$($MAIN_MENU --title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --column "$THEME_COLUMN_TITLE" --window-icon="$ICON_PATH" \
	"$GTK_LIGHT_ITEM" "$GTK_DARK_ITEM" "$KVANTUM_LIGHT_ITEM" "$KVANTUM_DARK_ITEM" \
	"$ICON_LIGHT_ITEM" "$ICON_DARK_ITEM" "$QT_ICON_LIGHT_ITEM" "$QT_ICON_DARK_ITEM" \
	"$CURSOR_LIGHT_ITEM" "$CURSOR_DARK_ITEM")

	if [[ -z $MAIN_BOX ]]; then
		exit
	elif [[ $MAIN_BOX == $GTK_LIGHT_ITEM ]]; then
		TITLE="GTK light theme chooser"
		TEXT="Choose which theme you want to use as GTK light theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		GTK_BOX=$(find /usr/share/themes/*/gtk* -type f -name gtk.css | awk -F '/' {'print $5'} | sort -u | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $GTK_BOX ]]; then
				exit
			elif [[ $GTK_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/gtk_theme_light/ s/$GTK_THEME_LIGHT/$GTK_BOX/" "$CONFIG_PATH"
				sidemenu_light_config
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/gtk_theme_light/ s/$GTK_THEME_LIGHT/$GTK_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_BOX"
				xfconf-query -c xfwm4 -p /general/theme -s "$GTK_BOX"
				sidemenu_light_config
				exec rb-dark-mode.sh -S
			else
				echo "GTK light menu error"
			fi
	elif [[ $MAIN_BOX == $GTK_DARK_ITEM ]]; then
		TITLE="GTK dark theme chooser"
		TEXT="Choose which theme you want to use as GTK dark theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		GTK_BOX=$(find /usr/share/themes/*/gtk* -type f -name gtk.css | awk -F '/' {'print $5'} | sort -u | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $GTK_BOX ]]; then
				exit
			elif [[ $GTK_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/gtk_theme_dark/ s/$GTK_THEME_DARK/$GTK_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_BOX"
				xfconf-query -c xfwm4 -p /general/theme -s "$GTK_BOX"
				sidemenu_dark_config
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/gtk_theme_dark/ s/$GTK_THEME_DARK/$GTK_BOX/" "$CONFIG_PATH"
				sidemenu_dark_config
				exec rb-dark-mode.sh -S
			else
				echo "GTK dark menu error"
			fi
	elif [[ $MAIN_BOX == $KVANTUM_LIGHT_ITEM ]]; then
		TITLE="Kvantum light theme chooser"
		TEXT="Choose which theme you want to use as Kvantum light theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		KV_BOX=$(ls -d /usr/share/Kvantum/* | awk -F '/' {'print $5'} | $ZEN_MENU --title="$TITLE" \
		--text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $KV_BOX ]]; then
				exit
			elif [[ $KV_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/kvantum_theme_light/ s/$KVANTUM_THEME_LIGHT/$KV_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/kvantum_theme_light/ s/$KVANTUM_THEME_LIGHT/$KV_BOX/" "$CONFIG_PATH"
				kvantummanager --set "$KV_BOX"
				exec rb-dark-mode.sh -S
			else
				echo "Kvantum light menu error"
			fi
	elif [[ $MAIN_BOX == $KVANTUM_DARK_ITEM ]]; then
		TITLE="Kvantum dark theme chooser"
		TEXT="Choose which theme you want to use as Kvantum dark theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		KV_BOX=$(ls -d /usr/share/Kvantum/* | awk -F '/' {'print $5'} | $ZEN_MENU --title="$TITLE" \
		--text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $KV_BOX ]]; then
				exit
			elif [[ $KV_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/kvantum_theme_dark/ s/$KVANTUM_THEME_DARK/$KV_BOX/" "$CONFIG_PATH"
				kvantummanager --set "$KV_BOX"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/kvantum_theme_dark/ s/$KVANTUM_THEME_DARK/$KV_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			else
				echo "Kvantum dark menu error"
			fi
	elif [[ $MAIN_BOX == $ICON_LIGHT_ITEM ]]; then
		TITLE="Icons light theme chooser"
		TEXT="Choose which theme you want to use as Icon light theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
		$HOME/.local/share/icons/*/index.theme | awk -F '/' {'print $5'} | sort -u | grep -v hicolor | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $ICON_BOX ]]; then
				exit
			elif [[ $ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/icons_theme_light/ s/$ICON_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/icons_theme_light/ s/$ICON_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_BOX"
				exec rb-dark-mode.sh -S
			else
				echo "Icon light menu error"
			fi
	elif [[ $MAIN_BOX == $ICON_DARK_ITEM ]]; then
		TITLE="Icons dark theme chooser"
		TEXT="Choose which theme you want to use as Icon dark theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
		$HOME/.local/share/icons/*/index.theme | awk -F '/' {'print $5'} | sort -u | grep -v hicolor | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $ICON_BOX ]]; then
				exit
			elif [[ $ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/icons_theme_dark/ s/$ICON_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_BOX"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/icons_theme_dark/ s/$ICON_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			else
				echo "Icon light menu error"
			fi
	elif [[ $MAIN_BOX == $QT_ICON_LIGHT_ITEM ]]; then
		TITLE="Qt Icon light theme chooser"
		TEXT="Choose which theme you want to use as Qt Icon light theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		QT_ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
		$HOME/.local/share/icons/*/index.theme | awk -F '/' {'print $5'} | sort -u | grep -v hicolor | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $QT_ICON_BOX ]]; then
				exit
			elif [[ $QT_ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/qt_icon_theme_light/ s/$QT_ICON_THEME_LIGHT/$QT_ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/qt_icon_theme_light/ s/$QT_ICON_THEME_LIGHT/$QT_ICON_BOX/" "$CONFIG_PATH"
				sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_BOX/" "$QT_CONFIG_PATH"
				exec rb-dark-mode.sh -S
			else
				echo "Qt Icon light menu error"
			fi
	elif [[ $MAIN_BOX == $QT_ICON_DARK_ITEM ]]; then
		TITLE="Qt Icon dark theme chooser"
		TEXT="Choose which theme you want to use as Qt Icon dark theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		QT_ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
		$HOME/.local/share/icons/*/index.theme | awk -F '/' {'print $5'} | sort -u | grep -v hicolor | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" \
		--separator=";")
			if [[ -z $QT_ICON_BOX ]]; then
				exit
			elif [[ $QT_ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/qt_icon_theme_dark/ s/$QT_ICON_THEME_DARK/$QT_ICON_BOX/" "$CONFIG_PATH"
				sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_BOX/" "$QT_CONFIG_PATH"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/qt_icon_theme_dark/ s/$QT_ICON_THEME_DARK/$QT_ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			else
				echo "Qt Icon dark menu error"
			fi
	elif [[ $MAIN_BOX == $CURSOR_LIGHT_ITEM ]]; then
		TITLE="Cursor light theme chooser"
		TEXT="Choose which theme you want to use as Cursor light theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		ICON_BOX=$(find /usr/share/icons/*/ $HOME/.icons/*/ \
		$HOME/.local/share/icons/*/ -type d -name cursors | awk -F '/' {'print $5'} | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH" --separator=";")
			if [[ -z $ICON_BOX ]]; then
				exit
			elif [[ $ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/cursor_theme_light/ s/$CURSOR_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/cursor_theme_light/ s/$CURSOR_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$ICON_BOX"
				exec rb-dark-mode.sh -S
			else
				echo "Cursor light menu error"
			fi
	elif [[ $MAIN_BOX == $CURSOR_DARK_ITEM ]]; then
		TITLE="Cursor dark theme chooser"
		TEXT="Choose which theme you want to use as Cursor dark theme."
		ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
		COLUMN_TITLE="Pick"

		ICON_BOX=$(find /usr/share/icons/*/ $HOME/.icons/*/ \
		$HOME/.local/share/icons/*/ -type d -name cursors | awk -F '/' {'print $5'} | $ZEN_MENU \
		--title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" \
		--column="$COLUMN_TITLE" --window-icon="$ICON_PATH" --separator=";")
			if [[ -z $ICON_BOX ]]; then
				exit
			elif [[ $ICON_BOX == $EXTRA_BUTTON ]]; then
				settings
			elif [[ $DARK_MODE_STATUS == "On" ]]; then
				sed -i "/cursor_theme_dark/ s/$CURSOR_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
				xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$ICON_BOX"
				exec rb-dark-mode.sh -S
			elif [[ $DARK_MODE_STATUS == "Off" ]]; then
				sed -i "/cursor_theme_dark/ s/$CURSOR_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
				exec rb-dark-mode.sh -S
			else
				echo "Cursor light menu error"
			fi
	else
		echo "Main menu: error"
	fi

}

run() {
GTK_THEME_LIGHT=$(grep -w gtk_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
GTK_THEME_DARK=$(grep -w gtk_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
KVANTUM_THEME_LIGHT=$(grep -w kvantum_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
KVANTUM_THEME_DARK=$(grep -w kvantum_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
ICON_THEME_LIGHT=$(grep -w icons_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
ICON_THEME_DARK=$(grep -w icons_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_ICON_THEME_LIGHT=$(grep -w qt_icon_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_ICON_THEME_DARK=$(grep -w qt_icon_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
QT_CONFIG_THEME=$(grep -w icon_theme $HOME/.config/qt5ct/qt5ct.conf | cut -d '=' -f 2)
CURSOR_THEME_LIGHT=$(grep -w cursor_theme_light $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
CURSOR_THEME_DARK=$(grep -w cursor_theme_dark $HOME/.config/darkmode-settings/config | cut -d '=' -f 2)
FONT_ICON_ON=$(grep -w font_icon_on .config/darkmode-settings/config | cut -d '=' -f 2)
FONT_ICON_OFF=$(grep -w font_icon_off .config/darkmode-settings/config | cut -d '=' -f 2)
	if [[ $DARK_MODE_STATUS == "On" ]]; then
		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_THEME_LIGHT
		xfconf-query -c xfwm4 -p /general/theme -s $GTK_THEME_LIGHT
		kvantummanager --set $KVANTUM_THEME_LIGHT
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICON_THEME_LIGHT
		xfconf-query -c xsettings -p /Gtk/CursorThemeName -s $CURSOR_THEME_LIGHT
		sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_THEME_LIGHT/" "$QT_CONFIG_PATH"
		sed -i '/dark_mode_status/ s/On/Off/' "$CONFIG_PATH"
		sed -i '/side_menurc/ s/sidemenu-darkrc/sidemenu-lightrc/' "$CONFIG_PATH"
		sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_CONFIG_PATH"
		notify-send  --icon=recbox-dark-mode -t 3000 "Dark Mode Off"
	elif [[ $DARK_MODE_STATUS == "Off" ]]; then
		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_THEME_DARK
		xfconf-query -c xfwm4 -p /general/theme -s $GTK_THEME_DARK
		kvantummanager --set $KVANTUM_THEME_DARK
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICON_THEME_DARK
		xfconf-query -c xsettings -p /Gtk/CursorThemeName -s $CURSOR_THEME_DARK
		sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_THEME_DARK/" "$QT_CONFIG_PATH"
		sed -i '/dark_mode_status/ s/Off/On/' "$CONFIG_PATH"
		sed -i '/side_menurc/ s/sidemenu-lightrc/sidemenu-darkrc/' "$CONFIG_PATH"
		sed -i "s/$FONT_ICON_OFF/$FONT_ICON_ON/" "$SIDEMENU_CONFIG_PATH"
		notify-send  --icon=recbox-dark-mode -t 3000 "Dark Mode On"
	fi
}

error_dialog() {
zenity --error --title="Dark Mode - Settings" --text="Synchronization of config files error.\nCheck <i>settings_sync</i> in <b>rb-dark-mode.sh</b>." --no-wrap
}

case "$1" in

	--settings-sync | -s ) settings_sync;;
	--settings | -S ) settings;;
	-sS ) settings_sync && settings || error_dialog;;
	sidemenu_light_config ) sidemenu_light_config;;
	sidemenu_dark_config ) sidemenu_dark_config;;
	--run | -r ) run;;
	--version | -v ) echo -e "\n    Version 1.0\n";;
	*)

echo -e "
	Usage:\n
	rb-dark-mode.sh [ OPTION ]\n
	Options:\n
	 -s, --settings-sync    Synchronizing themes settings with config file
				in ~/.config/in dark-mode-settings/ directory.
	 -S, --settings         Open Settings GUI.
	 -r, --run              Run Redshift in One-Shot mode.
	 -v, --version
" >&2
exit 1;;
esac
exit 0
