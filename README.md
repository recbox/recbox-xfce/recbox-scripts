## RecBox tools for Manjaro XFCE
* **Workflow** - Zenity UI and scripts to setup autostart, services and tweaking system
* **Night Light** - Zenity UI and scripts to run Redshift in One-Shot Mode
* **Dark Mode** - Zenity UI and scripts to set themes choosen by user
* **Side Menu** -  customized jgmenu to perform like side panel
* **Shortcuts Menu** - Zenity UI with list of shortcuts
* **Other small helper scripts**

## Translation
Directories containing translations can be found here:
* [**usr/share/workflow/translations/run**](https://gitlab.com/recbox/recbox-xfce/recbox-scripts/-/tree/master/usr/share/workflow/translations/run)
- [x] en_US
- [x] pl_PL
* [**usr/share/workflow/translations/settings**](https://gitlab.com/recbox/recbox-xfce/recbox-scripts/-/tree/master/usr/share/workflow/translations/settings)
- [x] en_US
- [x] pl_PL
* [**/usr/share/workflow/translations/system**](https://gitlab.com/recbox/recbox-xfce/recbox-scripts/-/tree/master/usr/share/workflow/translations/system)
- [x] en_US
- [x] pl_PL
* [**/usr/share/xfce-shortcuts**](https://gitlab.com/recbox/recbox-xfce/recbox-scripts/-/tree/master/usr/share/xfce-shortcuts)
- [x] de_DE
- [x] en_US
- [x] fr_FR
- [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` file and rename it (replace `.utf8` with `.trans`)
* replace text under quotation marks
